package com.cipl.ParserClasses;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

/**
 * Class to Get JSON Data from URL.
 *
 */
public class GetJsonArrayClass {

	/**Method to get JSON Array from URL.
	 * @param url source url for JSON data.
	 * @return JSON Array containing data in JSON Format.
	 */
	public  static  JSONArray  getJSONArrayfromURL(String url) 
	{
		InputStream mIs = null;
		String result = "";
		JSONArray jArrayEvent = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try{
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = bufferReader.readLine()) != null) {
				if(line.trim().equals("\n"))
					continue;
				stringBuilder.append(line + "\n");
			}
			mIs.close();
			result=stringBuilder.toString();
		}catch(Exception e){

			Log.e("log_tag", "Error converting result "+e.toString());
		}
		try {
			jArrayEvent = new JSONArray(result);
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return jArrayEvent;
	}
}