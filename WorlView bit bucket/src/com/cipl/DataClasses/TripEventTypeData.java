package com.cipl.DataClasses;
/**
 * Data Class for Trip Event happened in a Trip
 */
public class TripEventTypeData {

	private String RecordID;
	private String AssetID;
	private String PersonID;
	private String EventTypeID;
	private String DeviceID;
	private String Address;
	private double Latitude;
	private double Longitude;
	private String DeviceTypeID;
	private String Source;
	private String ESN;
	private String PersonName;
	private String AssetName;
	private String EventTypeName;
	private String EventTime;
	private String Acceleration;
	private String Speed;
	private String Bearing;
	private String AccelDuration;

	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */

	public String getRecordID() {
		return RecordID;
	}
	public void setRecordID(String recordID) {
		RecordID = recordID;
	}
	public String getAssetID() {
		return AssetID;
	}
	public void setAssetID(String assetID) {
		AssetID = assetID;
	}
	public String getPersonID() {
		return PersonID;
	}
	public void setPersonID(String personID) {
		PersonID = personID;
	}
	public String getEventTypeID() {
		return EventTypeID;
	}
	public void setEventTypeID(String eventTypeID) {
		EventTypeID = eventTypeID;
	}
	public String getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public double getLatitude() {
		return Latitude;
	}
	public void setLatitude(double latitude) {
		Latitude = latitude;
	}
	public double getLongitude() {
		return Longitude;
	}
	public void setLongitude(double longitude) {
		Longitude = longitude;
	}
	public String getDeviceTypeID() {
		return DeviceTypeID;
	}
	public void setDeviceTypeID(String deviceTypeID) {
		DeviceTypeID = deviceTypeID;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getESN() {
		return ESN;
	}
	public void setESN(String eSN) {
		ESN = eSN;
	}
	public String getPersonName() {
		return PersonName;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public String getAssetName() {
		return AssetName;
	}
	public void setAssetName(String assetName) {
		AssetName = assetName;
	}
	public String getEventTypeName() {
		return EventTypeName;
	}
	public void setEventTypeName(String eventTypeName) {
		EventTypeName = eventTypeName;
	}
	public String getEventTime() {
		return EventTime;
	}
	public void setEventTime(String eventTime) {
		EventTime = eventTime;
	}
	public String getAcceleration() {
		return Acceleration;
	}
	public void setAcceleration(String acceleration) {
		Acceleration = acceleration;
	}
	public String getSpeed() {
		return Speed;
	}
	public void setSpeed(String speed) {
		Speed = speed;
	}
	public String getBearing() {
		return Bearing;
	}
	public void setBearing(String bearing) {
		Bearing = bearing;
	}
	public String getAccelDuration() {
		return AccelDuration;
	}
	public void setAccelDuration(String accelDuration) {
		AccelDuration = accelDuration;
	}


}
