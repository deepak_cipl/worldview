package com.cipl.AdapterClasses;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cipl.indexingclasses.GlobalTagItemComparator;
import com.cipl.indexingclasses.GlobalTagSectionIndexer;
import com.cipl.indexingclasses.GlobalTagsItemInterface;
import com.cipl.worldviewfinal.R;

/**
 *Customized Array Adapter class to show Objects list in Alphabetical order and divided by sections. 
 *
 */
public class GlobalTagListAdapter extends ArrayAdapter<GlobalTagsItemInterface>
{
	private int resource; // store the resource layout id for 1 row
	private boolean inSearchMode = false;

	private GlobalTagSectionIndexer indexer = null;

	/**Constructor for Adapter to pass ArrayList of Global Tags objects.
	 * @param _context Context of Activity which is showing the list.
	 * @param _resource Resource id for the List view.
	 * @param _items list of Items to show
	 */
	public GlobalTagListAdapter(Context _context, int _resource, List<GlobalTagsItemInterface> _items) {
		super(_context, _resource, _items);
		resource = _resource;

		// need to sort the items array first, then pass it to the indexer
		Collections.sort(_items, new GlobalTagItemComparator());
		setIndexer(new GlobalTagSectionIndexer(_items));

	}

	/**Method to return text view for sections.
	 * @param rowView
	 * @return  text view returned from Row View.
	 */
	public TextView getSectionTextView(View rowView){

		TextView sectionTextView = (TextView)rowView.findViewById(R.id.sectionTextViewHomesafeRow);
		return sectionTextView;
	}

	/**Method to show section Alphabet item at before the first Item.
	 * @param rowView Parent view
	 * @param item 
	 * @param position position of view
	 */
	public void showSectionViewIfFirstItem(View rowView, GlobalTagsItemInterface item, int position)
	{
		TextView sectionTextView = getSectionTextView(rowView);

		// if in search mode then dun show the section header
		if(inSearchMode){
			sectionTextView.setVisibility(View.GONE);
		}
		else
		{
			// if first item then show the header

			if(indexer.isFirstItemInSection(position)){

				String sectionTitle = indexer.getSectionTitle(item.getItemForIndex());
				sectionTextView.setText(sectionTitle);
				sectionTextView.setVisibility(View.VISIBLE);

			}
			else
				sectionTextView.setVisibility(View.GONE);
		}
	}


	/**
	 * method to do all the data population for the row here.
	 * subclass overwrite this to draw more items.
	 * @param parentView
	 * @param item
	 * @param position
	 */
	public void populateDataForRow(View parentView, GlobalTagsItemInterface item , int position){
		// default just draw the item only
		View infoView = parentView.findViewById(R.id.infoRowContainerHomesafeRow);
		TextView nameView = (TextView)infoView.findViewById(R.id.tv_row_objectHomesafeRow);
		nameView.setText(item.getItemForIndex());
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewGroup parentView;

		GlobalTagsItemInterface item = getItem(position);


		if (convertView == null) {
			parentView = new LinearLayout(getContext()); // Assumption: the resource parent id is a linear layout
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
			vi.inflate(resource, parentView, true);
		} else {
			parentView = (LinearLayout) convertView;
		}

		// for the very first section item, we will draw a section on top
		showSectionViewIfFirstItem(parentView, item, position);

		populateDataForRow(parentView, item, position);

		return parentView;

	}

	/**Check if list is in search mode.
	 * @return if list is in search mode.
	 */
	public boolean isInSearchMode() {
		return inSearchMode;
	}

	/**Set the list in search mode
	 * @param inSearchMode
	 */
	public void setInSearchMode(boolean inSearchMode) {
		this.inSearchMode = inSearchMode;
	}

	public GlobalTagSectionIndexer getIndexer() {
		return indexer;
	}

	/**set the indexer bar.
	 * @param globalTagSectionIndexer
	 */
	public void setIndexer(GlobalTagSectionIndexer globalTagSectionIndexer) {
		this.indexer = globalTagSectionIndexer;
	}

}
