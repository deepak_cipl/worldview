package com.cipl.ParserClasses;

import java.util.ArrayList;

import com.cipl.DataClasses.StaticData;
import com.cipl.worldviewfinal.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
/**
 * Custom Adapter class to show groups data in Arranged Group List.
 */
@SuppressLint("DefaultLocale")
public class CustomGroupAdapter extends BaseAdapter {
	//ArrayList<Boolean> checked_status=new ArrayList<Boolean>();
	Context context;
	//
	ArrayList<GroupFieldsData> show_list;
	/**Constructor for Group Adapter.
	 * @param context calling
	 * @param list Group Array List to be shown.
	 */
	public CustomGroupAdapter(Context context,ArrayList<GroupFieldsData> list) {
		this.context = context;
		show_list=list;
		//System.out.println("Show_list address..............."+show_list);
	}
	@Override
	public int getCount() 
	{
		return show_list.size();
	}
	@Override
	public Object getItem(int position) 
	{
		return position;
	}
	@Override
	public long getItemId(int position)
	{

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		ViewHolder holder;

		holder = new ViewHolder();
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView=inflater.inflate(R.layout.group_row_layout, null);

		holder.tv_groupName=(TextView)convertView.findViewById(R.id.tv_row_group);
		holder.chk_group_select=(CheckBox)convertView.findViewById(R.id.chk_row_group);

		holder.chk_group_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
			{

				int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
				show_list.get(getPosition).setSelected(buttonView.isChecked()); // Set the value of checkbox to maintain its state.

				if(isChecked){
					StaticData.groupIdSelected.add(show_list.get(getPosition).getGroupId());

				}else{
					StaticData.groupIdSelected.remove(show_list.get(getPosition).getGroupId());
				}
			}
		});
		convertView.setTag(holder);
		convertView.setTag(R.id.tv_row_group,holder.tv_groupName);
		convertView.setTag(R.id.chk_row_group, holder.chk_group_select);
		{
			holder = (ViewHolder)convertView.getTag();
		}
		int init_padding=20;
		Typeface externalFont = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaBQ-Medium.otf");
		holder.tv_groupName.setTypeface(externalFont);
		if((context.getResources().getConfiguration().screenLayout& Configuration.SCREENLAYOUT_SIZE_MASK)>= Configuration.SCREENLAYOUT_SIZE_LARGE)
		{
			holder.tv_groupName.setTextSize((30-show_list.get(position).level*1));
			holder.tv_groupName.setPadding(init_padding+show_list.get(position).level*30, 0, 0,0);
			holder.tv_groupName.setGravity(Gravity.CENTER_VERTICAL);
			holder.tv_groupName.setText( show_list.get(position).getGroupName().substring(0, 1).toUpperCase() +  show_list.get(position).getGroupName().substring(1));
		}
		else
		{
			holder.tv_groupName.setTextSize((20-show_list.get(position).level*1));
			holder.tv_groupName.setPadding(init_padding+show_list.get(position).level*15, 0, 0,0);
			holder.tv_groupName.setText( show_list.get(position).getGroupName().substring(0, 1).toUpperCase() +  show_list.get(position).getGroupName().substring(1));
		}

		holder.chk_group_select.setTag(position); // This line is important.

		holder.tv_groupName.setText(show_list.get(position).getGroupName());
		if(StaticData.groupIdSelected.contains(show_list.get(position).getGroupId()))
		{
			show_list.get(position).setSelected(true);
			holder.chk_group_select.setChecked(true);
		}	
		return convertView;
	}
	class ViewHolder
	{
		private TextView tv_groupName;
		private CheckBox chk_group_select;
	}





}
