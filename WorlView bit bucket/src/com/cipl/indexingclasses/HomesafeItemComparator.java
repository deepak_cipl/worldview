package com.cipl.indexingclasses;

import java.util.Comparator;

/**
 *Comparator Class of Home safe data to sort data alphabetically.
 */
public class HomesafeItemComparator implements Comparator<HomesafeItemInterface> {

	@Override
	public int compare(HomesafeItemInterface lhs, HomesafeItemInterface rhs) {
		if(lhs.getItemForIndex() == null || rhs.getItemForIndex() == null)
			return -1;


		return(lhs.getItemForIndex().compareTo(rhs.getItemForIndex() ) );

	}
}
