package com.cipl.AdapterClasses;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.cipl.DataClasses.GlobalTagsData;
import com.cipl.indexingclasses.GlobalTagsItemInterface;
import com.cipl.worldviewfinal.R;

/**
 * Class to do all the data population  draw more items
 *
 */
@SuppressLint("ViewConstructor")
public class GlobalTagRowAdapter extends GlobalTagListAdapter {

	public GlobalTagRowAdapter(Context _context, int _resource,List<GlobalTagsItemInterface> h_contactList) {
		super(_context, _resource, h_contactList);

	}

	public void populateDataForRow(View parentView, GlobalTagsItemInterface item , int position){
		// default just draw the item only
		// override this for custom drawing
		View infoView = parentView.findViewById(R.id.infoRowContainerHomesafeRow);
		TextView objectNameView = (TextView)infoView.findViewById(R.id.tv_row_objectHomesafeRow);

		objectNameView.setText(item.getItemForIndex());

		if(item instanceof GlobalTagsData)
		{
			GlobalTagsData contactItem = (GlobalTagsData)item;

			objectNameView.setText(contactItem.getItemForIndex());

		}

	}

}