
package com.cipl.HierarchyClasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import com.cipl.ParserClasses.GroupFieldsData;
import com.cipl.ParserClasses.ParsedGroupNameData;

/**
 * Class to Arrange Group Data and set hierarchy according to the level.
 */
public class MakeHierarchy
{

	ArrayList<GroupFieldsData> temp_list=new ArrayList<GroupFieldsData>();
	ArrayList<GroupFieldsData> temp;
	ArrayList<String> parent_keys_arr=new ArrayList<String>();
	ArrayList<String> unique_parent_id_list=new ArrayList<String>();
	HashMap<String,ArrayList<GroupFieldsData>> hmap=new LinkedHashMap<String, ArrayList<GroupFieldsData>>();
	HashMap<String,ArrayList<GroupFieldsData>> temp_hmap=new LinkedHashMap<String, ArrayList<GroupFieldsData>>();

	int x,max_level;

	/** Methods sort the data objects contains in hash map and store the data in temporary list. 
	 * this method is based on recursion. 
	 * @param data Parent GroupFieldsData object to start sorting
	 * @param level Level to be set on GroupFieldsData object.0 level is set for root parent object. 
	 */
	private void sortListAccordingToLevel(GroupFieldsData data,int level)
	{
		int size=hmap.get(data.getGroupId()).size();
		for(int c=0;c<size;c++){
			hmap.get(data.getGroupId()).get(c).level=level;
			temp_list.add(hmap.get(data.getGroupId()).get(c));
			if(hmap.get(data.getGroupId()).get(c).hasChild==true)
				sortListAccordingToLevel(hmap.get(data.getGroupId()).get(c),level+1);
		}
		return;
	}

	/**
	 * Method to generate Hierarchy for Group data.
	 * @return ArrayList of GroupFieldsData in which GroupFieldsData objects arranged in hierarchical form.
	 */
	public ArrayList<GroupFieldsData> getData()
	{
		temp=new ArrayList<GroupFieldsData>();
		if(ParsedGroupNameData.groupStorage.size()==0)
			return ParsedGroupNameData.groupStorage;
		temp.addAll(ParsedGroupNameData.groupStorage);
		int b=temp.size();
		//total parent ids
		for(int i=0;i<b;i++)
		{
			parent_keys_arr.add(temp.get(i).getParentGroupId());
		}
		for(int i=0;i<temp.size();i++)
		{
			for(String unique_val:parent_keys_arr)
			{
				if(!unique_parent_id_list.contains(unique_val))
				{
					unique_parent_id_list.add(unique_val);
				}
			}
		}

		for(int i=0;i<unique_parent_id_list.size();i++)
		{
			ArrayList<GroupFieldsData> parent_id_wise_data_local=new ArrayList<GroupFieldsData>();
			String parent_id=unique_parent_id_list.get(i);
			for(int j=0;j<temp.size();j++)
			{
				if((temp.get(j).getParentGroupId()).equals(parent_id))
				{
					if(unique_parent_id_list.contains(temp.get(j).getGroupId()))
						temp.get(j).hasChild=true;
					else
						temp.get(j).hasChild=false;
					parent_id_wise_data_local.add(temp.get(j));
				}
			}
			ArrayList<GroupFieldsData> t=new ArrayList<GroupFieldsData>();
			t.addAll(parent_id_wise_data_local);
			hmap.put(parent_id,t);
			parent_id_wise_data_local=null;
		}
		hmap.get("null").get(0).level=0;
		temp_list.add(hmap.get("null").get(0));
		sortListAccordingToLevel(hmap.get("null").get(0),1);
		return temp_list;
	}
}
