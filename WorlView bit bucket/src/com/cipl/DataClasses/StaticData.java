package com.cipl.DataClasses;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;


import com.cipl.ParserClasses.GroupFieldsData;
import com.cipl.indexingclasses.RovrItemInterface;

/**
 * Class to store static data for Application.
 */
public class StaticData {
	public static ArrayList<GroupFieldsData> groupList;
	//	public static ArrayList<String> groupIdSelected;
	public static LinkedHashSet<String> groupIdSelected;

	public static final String USER_NAME_STRING="UserName";
	public static final String USER_PASSWORD_STRING="Password";
	public static final String USER_LOGIN_FLAG_STRING="UserLoginFlag";
	public static LinkedHashSet<String> groupAdded,groupDeleted;
	//	public static ArrayList<TripData> data=new ArrayList<TripData>();
	public static int selectedTab;
	public static List<RovrItemInterface> rovrListInSearchMode;
	public static boolean hasRecentData;


	/**
	 * Get the name to show in list for Home safe Data.
	 * @param homeSafeData
	 * @return
	 */
	static String getValidNameHomeSafeData(HomesafeData homeSafeData){
		if(homeSafeData.getPerson_name().trim().equals("") || homeSafeData.getPerson_name().equals("null"))
		{
			if(homeSafeData.getAsset_name().trim().equals("") || homeSafeData.getAsset_name().equals("null"))
			{
				return homeSafeData.getDevice_name();
			}else
			{
				return homeSafeData.getAsset_name();
			}
		}
		else
		{
			return homeSafeData.getPerson_name();
		}
	}
	/**
	 * Get the Name representation for ROVR Data.
	 * @param rovrData
	 * @return
	 */
	static String getValidNameROVRSafeData(RovrData rovrData){

		if(rovrData.getPerson_name().trim().equals("") || rovrData.getPerson_name().equals("null"))
		{
			if(rovrData.getAsset_name().trim().equals("") || rovrData.getAsset_name().equals("null"))
			{
				return rovrData.getDevice_name();
			}else
			{
				return rovrData.getAsset_name();
			}
		}
		else
		{
			return rovrData.getPerson_name();
		}
	}
	/**Get the Name representation for GlobalTag Data.
	 * @param globalTagData
	 * @return
	 */
	static String getValidNameGlobalTagData(GlobalTagsData globalTagData){
		if(globalTagData.getPerson_name().trim().equals("") || globalTagData.getPerson_name().equals("null"))
		{
			if(globalTagData.getAsset_name().trim().equals("") || globalTagData.getAsset_name().equals("null"))
			{
				return globalTagData.getDevice_name();
			}else
			{
				return globalTagData.getAsset_name();
			}
		}
		else
		{
			return globalTagData.getPerson_name();
		}
	}
}
