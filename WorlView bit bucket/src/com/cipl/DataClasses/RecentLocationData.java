package com.cipl.DataClasses;

/**
 * Data Class to Show recent location.
 *
 */
public class RecentLocationData
{
	private String dateTime;
	private String latitude;
	private String longitude;
	private String speed;
	private String altitude;
	private String gpsAccuracy;
	private String eventType;
	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */



	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getGpsAccuracy() {
		return gpsAccuracy;
	}
	public void setGpsAccuracy(String gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

}
