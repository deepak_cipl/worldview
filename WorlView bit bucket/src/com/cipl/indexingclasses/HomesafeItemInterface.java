package com.cipl.indexingclasses;

import com.cipl.DataClasses.HomesafeData;

/**
 * Interface for Homesafe section indexer.
 */
public interface HomesafeItemInterface {

	/**Return the item that we want to categorize under this index. It can be first_name or last_name or display_name
	  e.g. "Albert Tan" , "Amy Green" , "Alex Ferguson" will fall under index A
	  "Ben Alpha", "Ben Beta" will fall under index B 
	 * @return Return String Under Index.
	 */
	public String getItemForIndex();   
	public HomesafeData getDataForIndex();

}
