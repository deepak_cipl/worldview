package com.cipl.worldviewfinal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.cipl.AdapterClasses.GlobalTagRowAdapter;
import com.cipl.AdapterClasses.HomesafeRowAdapter;
import com.cipl.AdapterClasses.RovrRowAdapter;
import com.cipl.DataClasses.GlobalTagsData;
import com.cipl.DataClasses.HomesafeData;
import com.cipl.DataClasses.RovrData;
import com.cipl.DataClasses.StaticData;
import com.cipl.ParserClasses.ParseGlobalTags;
import com.cipl.ParserClasses.ParseHomesafe;
import com.cipl.ParserClasses.ParseRovr;
import com.cipl.ParserClasses.URLClass;
import com.cipl.Widgets.CustomProgressDialog;
import com.cipl.Widgets.ExtendedRovrListView;
import com.cipl.indexingclasses.HomesafeItemInterface;
import com.cipl.indexingclasses.RovrItemInterface;
import com.cipl.indexingclasses.GlobalTagsItemInterface;
@SuppressLint("DefaultLocale")
public class ObjectsActivity extends Activity implements OnClickListener,OnItemClickListener{
	// Single ListView
	ExtendedRovrListView lv_rovr_home_activity;
	RadioButton radioButton_rovr,radioButton_homesafe,radioButton_globaltag;
	CustomProgressDialog mCustomProgressDialog;
	Button btn_group_ObjectActivity,btn_refresh_ObjectActivity;
	EditText et_serchbox;
	int selected_tab=1;
	private Object rovrSearchLock = new Object();
	private Object homeSearchLock = new Object();
	static String TAG="OjectsActivity";
	String searchString;
	//For Rovr
	List<RovrItemInterface> r_contactList;
	List<RovrItemInterface> r_filterList;
	//For Homesafe
	List<HomesafeItemInterface> h_contactList;
	List<HomesafeItemInterface> h_filterList;
	//For GlobalTag
	List<GlobalTagsItemInterface> g_contactList;
	List<GlobalTagsItemInterface> g_filterList;
	//Initializing Adapters

	RovrRowAdapter r_adapter;
	HomesafeRowAdapter h_adapter;
	GlobalTagRowAdapter g_adapter;
	private SearchListTaskRovr rovrSearchTask = null;
	private SearchListTaskHome homeSearchTask = null;
	private SearchListTaskGlobalTags globaltagSearchTask = null;
	int HomesafePositionClicked_inObject;
	String personId;
	String deviceId;
	String assetId,name;
	String fromActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_objects);

		r_filterList = new ArrayList<RovrItemInterface>();
		r_contactList =ParseRovr.rovrStorage;

		h_filterList = new ArrayList<HomesafeItemInterface>();
		h_contactList =ParseHomesafe.homesafeStorage;

		g_filterList = new ArrayList<GlobalTagsItemInterface>();
		g_contactList =ParseGlobalTags.globalTagsStorage;
		init();

		new 	LoadChangedSelectedGroupData().execute();

		lv_rovr_home_activity.setFastScrollEnabled(true);
		lv_rovr_home_activity.setOnItemClickListener(this);
		et_serchbox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
			}

			@Override

			public void afterTextChanged(Editable arg0) {
				if(selected_tab==1)
				{
					searchString = et_serchbox.getText().toString().trim().toUpperCase();

					if(rovrSearchTask!=null && rovrSearchTask.getStatus() != AsyncTask.Status.FINISHED)
					{
						try{
							rovrSearchTask.cancel(true);
						}
						catch(Exception e){
							Log.i(TAG, "Fail to cancel running search task");
						}
					}

					rovrSearchTask = new SearchListTaskRovr();
					rovrSearchTask.execute(searchString); // put it in a task so that ui is not freeze
				}
				else if(selected_tab==2)
				{
					searchString = et_serchbox.getText().toString().trim().toUpperCase();

					if(homeSearchTask!=null && homeSearchTask.getStatus() != AsyncTask.Status.FINISHED)
					{
						try{
							homeSearchTask.cancel(true);
						}
						catch(Exception e){
							Log.i(TAG, "Fail to cancel running search task");
						}
					}
					homeSearchTask = new SearchListTaskHome();
					homeSearchTask.execute(searchString); // put it in a task so that ui is not freeze
				}
				else if(selected_tab==3)
				{
					searchString = et_serchbox.getText().toString().trim().toUpperCase();

					if(globaltagSearchTask!=null && globaltagSearchTask.getStatus() != AsyncTask.Status.FINISHED)
					{
						try{
							globaltagSearchTask.cancel(true);
						}
						catch(Exception e){
							Log.i(TAG, "Fail to cancel running search task");
						}
					}
					globaltagSearchTask = new SearchListTaskGlobalTags();
					globaltagSearchTask.execute(searchString); // put it in a task so that ui is not freeze
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after)
			{

			}
		});
	}

	/**
	 * Listener class to be notified on changing selection state of radio buttons at the  bottom bar.
	 */
	private CompoundButton.OnCheckedChangeListener btnNavBarOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked) 
			{
				if(buttonView==radioButton_rovr)
				{
					r_adapter = new RovrRowAdapter(ObjectsActivity.this,R.layout.object_rovr_row_layout, r_contactList);
					lv_rovr_home_activity.setAdapter(r_adapter);
					selected_tab=1;
					StaticData.selectedTab=1;

				}
				else if(buttonView==radioButton_homesafe)
				{

					h_adapter = new HomesafeRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, h_contactList);
					lv_rovr_home_activity.setAdapter(h_adapter);
					selected_tab=2;
					StaticData.selectedTab=2;
				}
				else if(buttonView==radioButton_globaltag)
				{
					g_adapter = new GlobalTagRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, g_contactList);
					lv_rovr_home_activity.setAdapter(g_adapter);
					selected_tab=3;
					StaticData.selectedTab=3;
				}
			}
		}
	};
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnGroupShowHomeActivity:
			startActivity(new Intent(ObjectsActivity.this,GroupActivity.class));
			finish();
			break;

		case R.id.btnRefreshHomeActivity:
			ParseHomesafe.homesafeStorage.clear();
			ParseRovr.rovrStorage.clear();
			ParseGlobalTags.globalTagsStorage.clear();
			new LoadChangedSelectedGroupData().execute();
			break;
		}
	}
	/**
	 *Initialize the views. 
	 */
	private void init() 
	{
		btn_group_ObjectActivity=(Button)findViewById(R.id.btnGroupShowHomeActivity);
		et_serchbox=(EditText)findViewById(R.id.editTxtSearchHomeActivity);
		radioButton_rovr = (RadioButton) findViewById(R.id.radioBtnRovrHomeActivity);
		radioButton_rovr.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
		radioButton_homesafe = (RadioButton) findViewById(R.id.radioBtnHomeSafeHomeActivity);
		radioButton_homesafe.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
		radioButton_globaltag = (RadioButton) findViewById(R.id.radioBtnGlobalTagHomeActivity);
		radioButton_globaltag.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
		et_serchbox.setOnClickListener(this);
		btn_group_ObjectActivity.setOnClickListener(this);
		btn_refresh_ObjectActivity=(Button)findViewById(R.id.btnRefreshHomeActivity);
		btn_refresh_ObjectActivity.setOnClickListener(this);
		lv_rovr_home_activity = (ExtendedRovrListView) this.findViewById(R.id.r_list_view_HomeActivity);
		//		groupAdded=getIntent().getParcelableExtra("groupAdded");
		//		groupDeleted=getIntent().getParcelableExtra("groupDeleted");

		//changes..........................................................................
		lv_rovr_home_activity.setItemsCanFocus(true);
		lv_rovr_home_activity.setFocusable(false);
		lv_rovr_home_activity.setFocusableInTouchMode(false);
		lv_rovr_home_activity.setClickable(false);
		getIntent();
		//		int intValue = mIntent.getIntExtra("selected_tab_sent", 1);
		selected_tab=StaticData.selectedTab;
		if(selected_tab==1)
		{
			radioButton_rovr.setChecked(true);
		}
		else if(selected_tab==2)
		{
			radioButton_homesafe.setChecked(true);
		}
		else if(selected_tab==3)
		{
			radioButton_globaltag.setChecked(true);
		}
	}

	// When user goes back from object activity to login activity
	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(ObjectsActivity.this,LoginActivity.class));
		finish();
	}
	/**
	 *Async task for search functionality.  
	 */
	private class SearchListTaskRovr extends AsyncTask<String, Void, String> {

		boolean inSearchMode = false;

		@Override
		protected String doInBackground(String... params) {
			r_filterList.clear();

			String keyword = params[0];

			inSearchMode = (keyword.length() > 0);

			if (inSearchMode) {
				// get all the items matching this
				for (RovrItemInterface item : r_contactList) {
					RovrData rovr = (RovrData)item;
					if ((rovr.getItemForIndex().toUpperCase().indexOf(keyword) > -1) ) {
						r_filterList.add(item);
					}

				}
			} 
			return null;
		}

		@SuppressLint("DefaultLocale")
		protected void onPostExecute(String result) {
			synchronized(rovrSearchLock)
			{
				if(inSearchMode)
				{
					RovrRowAdapter adapter = new RovrRowAdapter(ObjectsActivity.this, R.layout.object_rovr_row_layout, r_filterList);
					adapter.setInSearchMode(true);
					lv_rovr_home_activity.setInSearchMode(true);
					lv_rovr_home_activity.setAdapter(adapter);
				}
				else{
					RovrRowAdapter adapter = new RovrRowAdapter(ObjectsActivity.this, R.layout.object_rovr_row_layout, r_contactList);
					adapter.setInSearchMode(false);
					lv_rovr_home_activity.setInSearchMode(false);
					lv_rovr_home_activity.setAdapter(adapter);
				}
			}
		}
	}
	private class SearchListTaskHome extends AsyncTask<String, Void, String> {
		boolean inSearchMode = false;
		@Override
		protected String doInBackground(String... params) {
			h_filterList.clear();
			String keyword = params[0];
			inSearchMode = (keyword.length() > 0);
			if (inSearchMode) {
				// get all the items matching this
				for (HomesafeItemInterface item : h_contactList) {
					HomesafeData home = (HomesafeData)item;
					if ((home.getItemForIndex().toUpperCase().indexOf(keyword) > -1) ) {
						h_filterList.add(item);
					}
				}
			} 
			return null;
		}
		@SuppressLint("DefaultLocale")
		protected void onPostExecute(String result) {
			synchronized(homeSearchLock)
			{
				if(inSearchMode){
					HomesafeRowAdapter adapter = new HomesafeRowAdapter(ObjectsActivity.this, R.layout.object_homesafe_row_layout, h_filterList);
					adapter.setInSearchMode(true);
					lv_rovr_home_activity.setInSearchMode(true);
					lv_rovr_home_activity.setAdapter(adapter);
				}
				else{
					HomesafeRowAdapter adapter = new HomesafeRowAdapter(ObjectsActivity.this, R.layout.object_homesafe_row_layout, h_contactList);
					adapter.setInSearchMode(false);
					lv_rovr_home_activity.setInSearchMode(false);
					lv_rovr_home_activity.setAdapter(adapter);
				}
			}

		}
	}
	private class SearchListTaskGlobalTags extends AsyncTask<String, Void, String> {
		boolean inSearchMode = false;
		@Override
		protected String doInBackground(String... params) {
			h_filterList.clear();
			String keyword = params[0];
			inSearchMode = (keyword.length() > 0);
			if (inSearchMode) {
				// get all the items matching this
				for (GlobalTagsItemInterface item : g_contactList) {
					GlobalTagsData home = (GlobalTagsData)item;
					if ((home.getItemForIndex().toUpperCase().indexOf(keyword) > -1) ) {
						g_filterList.add(item);
					}
				}
			} 
			return null;
		}
		@SuppressLint("DefaultLocale")
		protected void onPostExecute(String result) {
			synchronized(homeSearchLock)
			{
				if(inSearchMode){
					GlobalTagRowAdapter adapter = new GlobalTagRowAdapter(ObjectsActivity.this, R.layout.object_homesafe_row_layout, g_filterList);
					adapter.setInSearchMode(true);
					lv_rovr_home_activity.setInSearchMode(true);
					lv_rovr_home_activity.setAdapter(adapter);
				}
				else{
					GlobalTagRowAdapter adapter = new GlobalTagRowAdapter(ObjectsActivity.this, R.layout.object_homesafe_row_layout, g_contactList);
					adapter.setInSearchMode(false);
					lv_rovr_home_activity.setInSearchMode(false);
					lv_rovr_home_activity.setAdapter(adapter);
				}
			}
		}
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(selected_tab==1)
		{

			personId=ParseRovr.rovrStorage.get(position).getDataForIndex().getPerson_id();
			deviceId=ParseRovr.rovrStorage.get(position).getDataForIndex().getDevice_id();
			assetId=ParseRovr.rovrStorage.get(position).getDataForIndex().getAsset_id();
			name=ParseRovr.rovrStorage.get(position).getItemForIndex();
			HomesafePositionClicked_inObject=position;
			fromActivity="ROVR";
		}

		if(selected_tab==2)
		{

			name=ParseHomesafe.homesafeStorage.get(position).getItemForIndex();
			personId=ParseHomesafe.homesafeStorage.get(position).getDataForIndex().getPerson_id();
			deviceId=ParseHomesafe.homesafeStorage.get(position).getDataForIndex().getDevice_id();
			assetId=ParseHomesafe.homesafeStorage.get(position).getDataForIndex().getAsset_id();
			HomesafePositionClicked_inObject=position;
			fromActivity="homeSAFE";
		}

		if(selected_tab==3)
		{
			name=ParseGlobalTags.globalTagsStorage.get(position).getItemForIndex();

			personId=ParseGlobalTags.globalTagsStorage.get(position).getDataForIndex().getPerson_id();
			deviceId=ParseGlobalTags.globalTagsStorage.get(position).getDataForIndex().getDevice_id();
			assetId=ParseGlobalTags.globalTagsStorage.get(position).getDataForIndex().getAsset_id();
			HomesafePositionClicked_inObject=position;
			fromActivity="globalTAG";
		}
		Bundle b=new Bundle();
		b.putString("name", name);
		b.putString("personId", personId);
		b.putString("deviceId", deviceId);
		b.putString("assetId", assetId);
		b.putString("fromActivity", fromActivity);

		Intent in =new Intent(ObjectsActivity.this,HomesafeActivity.class);
		in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		in.putExtra("HomesafePositionClicked", HomesafePositionClicked_inObject);
		in.putExtras(b);
		startActivity(in);
	}

	/**
	 *Async Task class to load Objects of Groups according to the selection of groups.
	 */
	private class LoadChangedSelectedGroupData extends AsyncTask<String, Void, String>
	{
		ArrayList<GlobalTagsItemInterface> globalTagsStorageDataAdded=new ArrayList<GlobalTagsItemInterface>(); 
		ArrayList<HomesafeItemInterface> homesafeDataAdded = new ArrayList<HomesafeItemInterface>();; 
		ArrayList<RovrItemInterface> rovrAddedDataAdded=new ArrayList<RovrItemInterface>();
		@Override
		protected void onPreExecute() {
			System.out.println("Loading..do in background");
			mCustomProgressDialog = CustomProgressDialog.createDialog(ObjectsActivity.this, "", "");
			mCustomProgressDialog.show();
			mCustomProgressDialog.setCancelable(false);
			System.out.println("In pre execute");
		}
		@SuppressLint("SimpleDateFormat")
		@Override
		protected String doInBackground(String... params) {
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("M-dd-yyyy");
			String date = df.format(c.getTime());

			ArrayList<String> groupDataAdded = new ArrayList<String>();
			groupDataAdded.addAll(StaticData.groupAdded);
			//			ParseGlobalTags.globalTagsStorage.clear();
			ParseHomesafe.homesafeStaticData.clear();
			ParseRovr.rovrStaticData.clear();

			for(int i=0;i<groupDataAdded.size();i++){

				ParseHomesafe pData=new ParseHomesafe();
				homesafeDataAdded=pData.parseHomesafeList(URLClass.homesafe_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());
				System.out.println("Calling HomesafeList Api........."+URLClass.homesafe_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());
				for(int a=0;a<homesafeDataAdded.size();a++){
					homesafeDataAdded.get(a).getDataForIndex().setGroup_id(groupDataAdded.get(i));
				}
				ParseRovr rData=new ParseRovr();
				rovrAddedDataAdded=rData.parseRovrList(URLClass.rovr_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());
				System.out.println("Calling ROVRLIst Api........."+URLClass.rovr_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());

				for(int b=0;b<rovrAddedDataAdded.size();b++){
					rovrAddedDataAdded.get(b).getDataForIndex().setGroup_id(groupDataAdded.get(i));
				}
				ParseGlobalTags gtData=new ParseGlobalTags();
				globalTagsStorageDataAdded=gtData.parseGlobalTagsList(URLClass.globalTag_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());
				System.out.println("Calling GlobalTag Api........."+URLClass.globalTag_url_method(groupDataAdded.get(i),LoginActivity.username_entered,date).replaceAll(" ", "%20").trim());
				for(int d=0;d<globalTagsStorageDataAdded.size();d++){
					globalTagsStorageDataAdded.get(d).getDataForIndex().setGroup_id(groupDataAdded.get(i));
				}
				ParseHomesafe.homesafeStorage.addAll(homesafeDataAdded);
				ParseRovr.rovrStorage.addAll(rovrAddedDataAdded);
				ParseGlobalTags.globalTagsStorage.addAll(globalTagsStorageDataAdded);
			}

			removeGroupsFromAllList();

			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			System.out.println("Loading..post execute");
			if(selected_tab==1)
			{
				r_adapter.notifyDataSetChanged();
				//				r_adapter = new RovrRowAdapter(ObjectsActivity.this,R.layout.object_rovr_row_layout, ParseRovr.rovrStorage);
				r_adapter = new RovrRowAdapter(ObjectsActivity.this,R.layout.object_rovr_row_layout, r_contactList);
				lv_rovr_home_activity.setAdapter(r_adapter);
			}
			else if(selected_tab==2)
			{
				h_adapter.notifyDataSetChanged();
				//				h_adapter = new HomesafeRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, ParseHomesafe.homesafeStorage);
				h_adapter = new HomesafeRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, h_contactList);
				lv_rovr_home_activity.setAdapter(h_adapter);
			}
			else if(selected_tab==3)
			{
				g_adapter.notifyDataSetChanged();
				g_adapter = new GlobalTagRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, g_contactList);
				//				g_adapter = new GlobalTagRowAdapter(ObjectsActivity.this,R.layout.object_homesafe_row_layout, ParseGlobalTags.globalTagsStorage);
				lv_rovr_home_activity.setAdapter(g_adapter);
			}
			mCustomProgressDialog.dismiss();
			System.out.println("Data Refreshed.");

		}
	}
	/**
	 * Method to remove data from lists when groups are unselected after selection previously.
	 */
	private void removeGroupsFromAllList(){

		ArrayList<GlobalTagsItemInterface> gS=new ArrayList<GlobalTagsItemInterface>(); 
		ArrayList<HomesafeItemInterface> hs = new ArrayList<HomesafeItemInterface>();
		ArrayList<RovrItemInterface> rs=new ArrayList<RovrItemInterface>();
		ArrayList<String>  groupDeleted=new ArrayList<String>();
		groupDeleted.addAll(StaticData.groupDeleted);
		for(int i=0;i<groupDeleted.size();i++){
			for(int j=0;j<ParseHomesafe.homesafeStorage.size();j++){
				if(ParseHomesafe.homesafeStorage.get(j).getDataForIndex().getGroup_id().equals(groupDeleted.get(i)))
					hs.add(ParseHomesafe.homesafeStorage.get(j));
			}
			for(int k=0;k<ParseRovr.rovrStorage.size();k++){
				if(ParseRovr.rovrStorage.get(k).getDataForIndex().getGroup_id().equals(groupDeleted.get(i)))
					rs.add(ParseRovr.rovrStorage.get(k));
			}
			for(int j=0;j<ParseGlobalTags.globalTagsStorage.size();j++){
				if(ParseGlobalTags.globalTagsStorage.get(j).getDataForIndex().getGroup_id().equals(groupDeleted.get(i)))
					gS.add(ParseGlobalTags.globalTagsStorage.get(j));
			}

			ParseHomesafe.homesafeStorage.removeAll(hs);
			ParseRovr.rovrStorage.removeAll(rs);
			ParseGlobalTags.globalTagsStorage.removeAll(gS);
		}

	}

}
