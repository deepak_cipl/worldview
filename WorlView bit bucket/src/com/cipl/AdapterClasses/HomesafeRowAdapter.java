package com.cipl.AdapterClasses;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.cipl.DataClasses.HomesafeData;
import com.cipl.indexingclasses.HomesafeItemInterface;
import com.cipl.worldviewfinal.R;
/**
 * Class to do all the data population  draw more items
 *
 */
public class HomesafeRowAdapter extends HomesafeListAdapter {

	public HomesafeRowAdapter(Context _context, int _resource,List<HomesafeItemInterface> h_contactList) {
		super(_context, _resource, h_contactList);

	}

	// override this for custom drawing
	public void populateDataForRow(View parentView, HomesafeItemInterface item , int position){
		// default just draw the item only
		View infoView = parentView.findViewById(R.id.infoRowContainerHomesafeRow);
		TextView objectNameView = (TextView)infoView.findViewById(R.id.tv_row_objectHomesafeRow);

		objectNameView.setText(item.getItemForIndex());

		if(item instanceof HomesafeData){
			HomesafeData contactItem = (HomesafeData)item;
			objectNameView.setText(contactItem.getItemForIndex());
		}
	}
}
