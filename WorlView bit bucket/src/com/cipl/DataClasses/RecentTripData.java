package com.cipl.DataClasses;

/**
 * Data Class for recent Trip Data.
 *
 */
public class RecentTripData 
{
	private String tripID;
	private String assetID;
	private String personID;
	private String deviceID;
	private String startingAddress;
	private String startingLat;
	private String startingLon;
	private String startingTime;
	private String endingAddress;
	private String duration;
	private String endingTime;
	private String endingLat;
	private String endingLon;
	private String ESN;
	private String personName;
	private String assetName;
	private String distance;
	private double TripOverallScore;
	private double AverageSpeed;
	private double MaxSpeed;
	private int BrakingEventsCount;
	private int SpeedingEventsCount;
	private int FastAccelEventsCount;



	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */
	public double getTripOverallScore() {
		return TripOverallScore;
	}
	public void setTripOverallScore(double tripOverallScore) {
		TripOverallScore = tripOverallScore;
	}
	public double getAverageSpeed() {
		return AverageSpeed;
	}
	public void setAverageSpeed(double averageSpeed) {
		AverageSpeed = averageSpeed;
	}
	public double getMaxSpeed() {
		return MaxSpeed;
	}
	public void setMaxSpeed(double maxSpeed) {
		MaxSpeed = maxSpeed;
	}
	public int getBrakingEventsCount() {
		return BrakingEventsCount;
	}
	public void setBrakingEventsCount(int brakingEventsCount) {
		BrakingEventsCount = brakingEventsCount;
	}
	public int getSpeedingEventsCount() {
		return SpeedingEventsCount;
	}
	public void setSpeedingEventsCount(int speedingEventsCount) {
		SpeedingEventsCount = speedingEventsCount;
	}
	public int getFastAccelEventsCount() {
		return FastAccelEventsCount;
	}
	public void setFastAccelEventsCount(int fastAccelEventsCount) {
		FastAccelEventsCount = fastAccelEventsCount;
	}

	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getTripID() {
		return tripID;
	}
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}
	public String getAssetID() {
		return assetID;
	}
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}
	public String getPersonID() {
		return personID;
	}
	public void setPersonID(String personID) {
		this.personID = personID;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getStartingAddress() {
		return startingAddress;
	}
	public void setStartingAddress(String startingAddress) {
		this.startingAddress = startingAddress;
	}
	public String getStartingLat() {
		return startingLat;
	}
	public void setStartingLat(String startingLat) {
		this.startingLat = startingLat;
	}
	public String getStartingLon() {
		return startingLon;
	}
	public void setStartingLon(String startingLon) {
		this.startingLon = startingLon;
	}
	public String getStartingTime() {
		return startingTime;
	}
	public void setStartingTime(String startingTime) {
		this.startingTime = startingTime;
	}
	public String getEndingAddress() {
		return endingAddress;
	}
	public void setEndingAddress(String endingAddress) {
		this.endingAddress = endingAddress;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getEndingTime() {
		return endingTime;
	}
	public void setEndingTime(String endingTime) {
		this.endingTime = endingTime;
	}
	public String getEndingLat() {
		return endingLat;
	}
	public void setEndingLat(String endingLat) {
		this.endingLat = endingLat;
	}
	public String getEndingLon() {
		return endingLon;
	}
	public void setEndingLon(String endingLon) {
		this.endingLon = endingLon;
	}
	public String getESN() {
		return ESN;
	}
	public void setESN(String eSN) {
		ESN = eSN;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

}
