package com.cipl.worldviewfinal;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Window;

public class SplashActivity extends Activity 
{
	String status;
	private final int SPLASH_DISPLAY_LENGTH = 2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		//	new LoadWholeData().execute();
		boolean isNetworkAvailable = false;
		ConnectivityManager cMgr = (ConnectivityManager) SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cMgr.getActiveNetworkInfo();
		if (netInfo != null) {
			isNetworkAvailable = netInfo.isConnectedOrConnecting();
		}
		if(isNetworkAvailable==true)
			status = netInfo.getState().toString();
		else
			status="DISCONNECTED";

		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				Intent i=new Intent(SplashActivity.this,LoginActivity.class);
				i.putExtra("CONNECTIVITY", status);
				startActivity(new Intent(i));
				SplashActivity.this.finish();
			}
		}, SPLASH_DISPLAY_LENGTH);
	}
}