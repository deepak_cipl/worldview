package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.GlobalTagsData;
import com.cipl.indexingclasses.GlobalTagsItemInterface;

/**
 *Class to parse global tag data from the Apis.
 */
public class ParseGlobalTags 
{
	public static ArrayList<GlobalTagsItemInterface> globalTagsStorage = new ArrayList<GlobalTagsItemInterface>();
	/**
	 * Method to parse Global Tags data in Static Array List. 
	 * @param url
	 */
	public ArrayList<GlobalTagsItemInterface> parseGlobalTagsList(String url)
	{
		ArrayList<GlobalTagsItemInterface> globalTagsStorageData=new ArrayList<GlobalTagsItemInterface>(); 
		JSONObject jsonObject;
		JSONArray jsonArray;
		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{
				return null ;
			}
			else
			{
				for(int i=0;i<jsonArray.length();i++)
				{
					GlobalTagsData gData = new GlobalTagsData();
					jsonObject=jsonArray.getJSONObject(i);
					String object_type=jsonObject.getString("ObjectType");
					gData.setObject_type(object_type);
					String device_id=jsonObject.getString("DeviceID");
					gData.setDevice_id(device_id);
					String device_name=jsonObject.getString("DeviceName");
					gData.setDevice_name(device_name);
					String asset_id=jsonObject.getString("AssetID");
					gData.setAsset_id(asset_id);
					String asset_name=jsonObject.getString("AssetName");
					gData.setAsset_name(asset_name);
					String person_id=jsonObject.getString("PersonId");
					gData.setPerson_id(person_id);
					String person_name=jsonObject.getString("PersonName");
					gData.setPerson_name(person_name);
					String object_event_type=jsonObject.getString("ObjectEventType");
					gData.setObject_event_type(object_event_type);
					String object_overall_score=jsonObject.getString("ObjectOverallScore");
					gData.setObjectOverallScore(object_overall_score);
					String ObjectDistanceDriven=jsonObject.getString("ObjectDistanceDriven");
					gData.setObjectOverallScore(ObjectDistanceDriven);
					String ObjectTotalMinutesIdling=jsonObject.getString("ObjectTotalMinutesIdling");
					gData.setObjectOverallScore(ObjectTotalMinutesIdling);
					//					globalTagsStorage.add(gData);					
					globalTagsStorageData.add(gData);	

				}
			}

		}
		catch (JSONException e) {

		}
		return globalTagsStorageData;
	}
}
