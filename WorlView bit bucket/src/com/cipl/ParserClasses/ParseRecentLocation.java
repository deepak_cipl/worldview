package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.RecentLocationData;
import com.cipl.DataClasses.StaticData;

/**
 *Class to parse recent location of objects.
 */
public class ParseRecentLocation {

	public static ArrayList<RecentLocationData>  recentLocationStorage=new ArrayList<RecentLocationData>();
	/**
	 * Method to parse HomeSafe data in Static Array List. 
	 * @param url
	 */
	public void parseRecentLocationList(String url)
	{
		JSONObject jsonObject;
		JSONArray jsonArray;

		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{
				StaticData.hasRecentData=false;
				return ;
			}
			else
			{

				for(int i=0;i<jsonArray.length();i++)
				{
					RecentLocationData hData = new RecentLocationData();

					jsonObject=jsonArray.getJSONObject(i);

					String date_time=jsonObject.getString("DateTime");
					hData.setDateTime(date_time);

					String latitude=jsonObject.getString("Latitude");
					hData.setLatitude(latitude);

					String longitude=jsonObject.getString("Longitude");
					hData.setLongitude(longitude);

					String speed=jsonObject.getString("Speed");
					hData.setSpeed(speed);

					String altitude=jsonObject.getString("Altitude");
					hData.setAltitude(altitude);

					String gpsAccuracy=jsonObject.getString("GPSaccuracy");
					hData.setGpsAccuracy(gpsAccuracy);

					String eventType=jsonObject.getString("EventType");
					hData.setEventType(eventType);

					recentLocationStorage.add(hData);
					StaticData.hasRecentData=true;
				}
			}
		}
		catch (JSONException e) {

		}
	}

}
