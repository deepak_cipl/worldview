package com.cipl.Widgets;

import com.cipl.worldviewfinal.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.widget.ImageView;
/**
 * Class to show customized progress Dialog bar.
 */
public class CustomProgressDialog extends Dialog {
	public CustomProgressDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		System.out.println("Focus changed..");
		ImageView imageView = (ImageView) findViewById(R.id.loadingImageView);
		AnimationDrawable yourAnimation = (AnimationDrawable) imageView.getBackground();
		yourAnimation.start();
	}
	/**Method to return Dialog box to be shown.
	 * @param context calling activity context.
	 * @param title Title of Dialog box.
	 * @param message Message to show on dialog Box.
	 * @return returns Custom Progress Dialog Box instance.
	 */
	public static CustomProgressDialog createDialog(Context context,String title, String message)
	{
		CustomProgressDialog dialog = null;
		if (title.length() == 0 || title == null) {
			dialog = new CustomProgressDialog(context,android.R.style.Theme_Panel);
		} else {
			dialog = new CustomProgressDialog(context,android.R.style.Theme_Panel);
		}

		dialog.setContentView(R.layout.dialogimg);
		dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		dialog.setCancelable(false);
		return dialog;
	}
}
