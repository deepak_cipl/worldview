package com.cipl.ParserClasses;

/**
 *Class for Group Fields Object.
 */
public class GroupFieldsData 
{ 
	private String groupId;
	private String groupName;
	private String parentGroupId;
	public boolean hasChild;
	public int level;
	private boolean selected;



	/**Getter method to get Group ID of Group Object.
	 * @return Group ID of Group Object
	 */
	public String getGroupId() {

		return groupId;
	}
	/**Setter method to set the Group ID value Group Object.
	 * @param groupId Group ID value Group Object
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	/**Getter method to get Group Name of Group Object.
	 * @return Group ID of Group Name
	 */
	public String getGroupName() {
		return groupName;
	}
	/**Setter method to set the Group Name value Group Object.
	 * @param groupName Group Name value Group Object
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**Getter method to get Parent Group ID of Group Object.
	 * @return Parent Group ID of Group Object
	 */
	public String getParentGroupId() {
		return parentGroupId;
	}
	/**Setter method to set the Parent Group ID value Group Object.
	 * @param parentGroupId Parent Group ID value Group Object
	 */
	public void setParentGroupId(String parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
