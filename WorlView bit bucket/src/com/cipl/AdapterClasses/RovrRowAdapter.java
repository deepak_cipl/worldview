package com.cipl.AdapterClasses;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.cipl.DataClasses.RovrData;
import com.cipl.indexingclasses.RovrItemInterface;
import com.cipl.worldviewfinal.R;

/**
 * Class to do all the data population  draw more items
 *
 */
@SuppressLint("ViewConstructor")
public class RovrRowAdapter extends RovrListAdapter {

	Context ctx;
	public RovrRowAdapter(Context _context, int _resource,List<RovrItemInterface> _items) {
		super(_context, _resource, _items);
		this.ctx=_context;

	}

	// override this for custom drawing
	public void populateDataForRow(View parentView, RovrItemInterface item , int position){
		// default just draw the item only
		View infoView = parentView.findViewById(R.id.infoRowContainer);
		TextView objectNameView = (TextView)infoView.findViewById(R.id.tv_row_object);

		objectNameView.setText(item.getItemForIndex());

		//Changed............................................................................................
		if(item instanceof RovrData)
		{
			RovrData contactItem = (RovrData)item;
			objectNameView.setText(contactItem.getItemForIndex());


		}
	}
}
