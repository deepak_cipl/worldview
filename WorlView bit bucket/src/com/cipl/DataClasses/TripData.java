package com.cipl.DataClasses;

import java.util.ArrayList;

/**
 * Class for Trip Data Object.
 */
public class TripData {


	private String TripID;
	private String AssetID;
	private String PersonID;
	private String DeviceID;
	private double StartingLat;
	private double  StartingLon;
	private double EndingLat;
	private double EndingLon;
	private String StartingTime;
	private String EndingTime;
	private String StartingAddress;
	private String EndingAddress;
	private String Duration;
	private String ESN;
	private String PersonName;
	private String AssetName;
	private String Distance;
	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */
	public double getTripOverallScore() {
		return TripOverallScore;
	}
	public void setTripOverallScore(double tripOverallScore) {
		TripOverallScore = tripOverallScore;
	}

	public int getBrakingEventsCount() {
		return BrakingEventsCount;
	}
	public void setBrakingEventsCount(int brakingEventsCount) {
		BrakingEventsCount = brakingEventsCount;
	}
	public int getSpeedingEventsCount() {
		return SpeedingEventsCount;
	}
	public void setSpeedingEventsCount(int speedingEventsCount) {
		SpeedingEventsCount = speedingEventsCount;
	}
	public int getFastAccelEventsCount() {
		return FastAccelEventsCount;
	}
	public void setFastAccelEventsCount(int fastAccelEventsCount) {
		FastAccelEventsCount = fastAccelEventsCount;
	}
	private String AverageSpeed;
	private String MaxSpeed;
	public String getAverageSpeed() {
		return AverageSpeed;
	}
	public void setAverageSpeed(String averageSpeed) {
		AverageSpeed = averageSpeed;
	}
	public String getMaxSpeed() {
		return MaxSpeed;
	}
	public void setMaxSpeed(String maxSpeed) {
		MaxSpeed = maxSpeed;
	}

	private ArrayList<TripEventTypeData> TripDetails;

	//////////////////////////////////////////////Below Data not added in trip Apis now.....that's why default to 0////////////////////////////////////////////

	private double TripOverallScore;

	private int BrakingEventsCount;
	private int SpeedingEventsCount;
	private int FastAccelEventsCount;

	public String getTripID() {
		return TripID;
	}
	public String getAssetID() {
		return AssetID;
	}
	public String getPersonID() {
		return PersonID;
	}
	public String getDeviceID() {
		return DeviceID;
	}
	public double getStartingLat() {
		return StartingLat;
	}
	public double getStartingLon() {
		return StartingLon;
	}
	public double getEndingLat() {
		return EndingLat;
	}
	public double getEndingLon() {
		return EndingLon;
	}
	public String getStartingTime() {
		return StartingTime;
	}
	public String getEndingTime() {
		return EndingTime;
	}
	public String getStartingAddress() {
		return StartingAddress;
	}
	public String getEndingAddress() {
		return EndingAddress;
	}
	public String getDuration() {
		return Duration;
	}
	public String getESN() {
		return ESN;
	}
	public String getPersonName() {
		return PersonName;
	}
	public String getAssetName() {
		return AssetName;
	}
	public String getDistance() {
		return Distance;
	}
	public ArrayList<TripEventTypeData> getTripDetails() {
		return TripDetails;
	}

	public void setTripID(String tripID) {
		TripID = tripID;
	}
	public void setAssetID(String assetID) {
		AssetID = assetID;
	}
	public void setPersonID(String personID) {
		PersonID = personID;
	}
	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}
	public void setStartingLat(double startingLat) {
		StartingLat = startingLat;
	}
	public void setStartingLon(double startingLon) {
		StartingLon = startingLon;
	}
	public void setEndingLat(double endingLat) {
		EndingLat = endingLat;
	}
	public void setEndingLon(double endingLon) {
		EndingLon = endingLon;
	}
	public void setStartingTime(String startingTime) {
		StartingTime = startingTime;
	}
	public void setEndingTime(String endingTime) {
		EndingTime = endingTime;
	}
	public void setStartingAddress(String startingAddress) {
		StartingAddress = startingAddress;
	}
	public void setEndingAddress(String endingAddress) {
		EndingAddress = endingAddress;
	}
	public void setDuration(String duration) {
		Duration = duration;
	}
	public void setESN(String eSN) {
		ESN = eSN;
	}
	public void setPersonName(String personName) {
		PersonName = personName;
	}
	public void setAssetName(String assetName) {
		AssetName = assetName;
	}
	public void setDistance(String distance) {
		Distance = distance;
	}
	public void setTripDetails(ArrayList<TripEventTypeData> tripDetails) {
		TripDetails = tripDetails;
	}

}
