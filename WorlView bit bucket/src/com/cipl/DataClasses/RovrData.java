package com.cipl.DataClasses;

import com.cipl.indexingclasses.RovrItemInterface;

/**
 * Class to save ROVR data in an object.
 *
 */
public class RovrData implements Comparable<RovrData>,RovrItemInterface
{
	private String object_type;
	private String device_id;
	private String device_name;
	private String asset_id;
	private String asset_name;
	private String person_id;
	private String person_name;
	private String object_event_type;
	private String ObjectOverallScore;
	private String ObjectDistanceDriven;
	private String ObjectTotalHoursDriven;
	private String group_id;


	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getObjectOverallScore() {
		return ObjectOverallScore;
	}
	public void setObjectOverallScore(String objectOverallScore) {
		ObjectOverallScore = objectOverallScore;
	}
	public String getObjectDistanceDriven() {
		return ObjectDistanceDriven;
	}
	public void setObjectDistanceDriven(String objectDistanceDriven) {
		ObjectDistanceDriven = objectDistanceDriven;
	}
	public String getObjectTotalHoursDriven() {
		return ObjectTotalHoursDriven;
	}
	public void setObjectTotalHoursDriven(String objectTotalHoursDriven) {
		ObjectTotalHoursDriven = objectTotalHoursDriven;
	}
	public String getObjectTotalMinutesIdling() {
		return ObjectTotalMinutesIdling;
	}
	public void setObjectTotalMinutesIdling(String objectTotalMinutesIdling) {
		ObjectTotalMinutesIdling = objectTotalMinutesIdling;
	}
	private String ObjectTotalMinutesIdling;


	public String getObject_type() {
		return object_type;
	}
	public void setObject_type(String object_type) {
		this.object_type = object_type;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public String getAsset_id() {
		return asset_id;
	}
	public void setAsset_id(String asset_id) {
		this.asset_id = asset_id;
	}
	public String getAsset_name() {
		return asset_name;
	}
	public void setAsset_name(String asset_name) {
		this.asset_name = asset_name;
	}
	public String getPerson_id() {
		return person_id;
	}
	public void setPerson_id(String person_id) {
		this.person_id = person_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getObject_event_type() {
		return object_event_type;
	}
	public void setObject_event_type(String object_event_type) {
		this.object_event_type = object_event_type;
	}

	@Override
	public int compareTo(RovrData another) {
		return StaticData.getValidNameROVRSafeData(this).compareTo(StaticData.getValidNameROVRSafeData(another));
		//		return asset_name.compareTo(another.asset_name);
	}
	@Override
	public String getItemForIndex() {
		// TODO Auto-generated method stub
		return StaticData.getValidNameROVRSafeData(this);
	}
	@Override
	public RovrData getDataForIndex() {
		// TODO Auto-generated method stub
		return this;
	}

}
