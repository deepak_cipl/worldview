package com.cipl.ParserClasses;

/**
 *Class for Api URLs
 */
public class URLClass 
{
	public static String login_url="http://devapi.cartasite.com/worldview/v1/groups?username=gkushnir55@cartasite.com";//Home URL for the Apis.



	/**Method returning the Api Url to be called according to the user entered.
	 * @param groupVariable User Name entered.
	 * @return string url to be called.
	 */
	public static String group_url_method(String groupVariable)
	{
		return "http://devapi.cartasite.com/worldview/v1/groups?token="+ groupVariable;
	}


	/**Method returning the Api Url to be called for ROVR list according to a particular group id.
	 * @param group_id group id of which ROVR List has to be shown.
	 * @param rovrVariable username entered.
	 * @param date Date for ROVR object List retrieval. 
	 * @return string url to be called
	 */
	public static String rovr_url_method(String group_id,String rovrVariable,String date)
	{

		return "http://devapi.cartasite.com/worldview/v1/objects?groupid="+group_id+"&objecttype=0&token="+ rovrVariable+"&startdatetime="+date+"&enddatetime="+date;
	}


	/**Method returning the Api Url to be called for homeSAFE list according to a particular group id.
	 * @param group_id group id of which homeSAFE List has to be shown.
	 * @param homesafeVariable username entered.
	 * @param date Date for homeSAFE object List retrieval. 
	 * @return string url to be called
	 */
	public static String homesafe_url_method(String group_id,String homesafeVariable,String date)
	{
		return "http://devapi.cartasite.com/worldview/v1/objects?groupid="+group_id+"&objecttype=1&token="+ homesafeVariable+"&startdatetime="+date+"&enddatetime="+date;
	}


	/**Method returning the Api Url to be called for globalTAG list according to a particular group id.
	 * @param group_id group id of which globalTAG List has to be shown.
	 * @param globalTagVariable username entered.
	 * @param date Date for globalTAG object List retrieval. 
	 * @return string url to be called
	 */
	public static String globalTag_url_method(String group_id,String globalTagVariable,String date)
	{
		return "http://devapi.cartasite.com/worldview/v1/objects?groupid="+group_id+"&objecttype=2&token="+globalTagVariable+"&startdatetime="+date+"&enddatetime="+date;
	}



	/**Method returning the Api Url to be called for Trips list according to the combination of group id,device id and person id.
	 * @param deviceid
	 * @param assetid
	 * @param personid
	 * @param token
	 * @param startdate
	 * @param enddate
	 * @return string url to be called.
	 */
	public static String tripData_url_method(String deviceid,String assetid,String personid,String token,String startdate,String enddate)
	{
		return "http://devapi.cartasite.com/worldview/v1/trips?deviceid="+deviceid+"&assetid="+assetid+"&personid="+personid+"&token="+token+"&startdatetime="+startdate+"&enddatetime="+enddate;
	}


	/**Method returning the Api Url to be called for Trip details events list according to the trip id.
	 * @param tripid
	 * @param token
	 * @return
	 */
	public static String tripDetailEventsData_url_method(String tripid,String token)
	{
		return "http://devapi.cartasite.com/worldview/v1/Trips/details?tripid="+tripid+"&token="+token;
	}

	/**Method returning the Api Url to be called for Trip details events list according to the trip id.
	 * @param deviceid
	 * @param assetid
	 * @param personid
	 * @param token
	 * @return string url to be called.
	 */
	public static String recentLocationData_url_method(String deviceid,String assetid,String personid,String token)
	{
		return "http://devapi.cartasite.com/worldview/v1/locations/recent?deviceid="+deviceid+"&assetid="+assetid+"&personid="+personid+"&token="+token;
	}
	/**Method returning the Api Url to be called for Trip details events list according to the trip id.
	 * @param deviceid
	 * @param assetid
	 * @param personid
	 * @param token
	 * @return string url to be called.
	 */
	public static String recentTripData_url_method(String deviceid,String assetid,String personid,String token)
	{
		return "http://devapi.cartasite.com/worldview/v1/trips/recent?deviceid="+deviceid+"&assetid="+assetid+"&personid="+personid+"&token="+token;
	}



}
