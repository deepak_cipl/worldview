package com.cipl.indexingclasses;

import com.cipl.DataClasses.GlobalTagsData;

public interface GlobalTagsItemInterface {

	public String getItemForIndex();   
	/**Get the Data on which GlobalTag Interface Implemented.
	 * @return Data Object for Global Tag Data.
	 */
	public  GlobalTagsData getDataForIndex();
}
