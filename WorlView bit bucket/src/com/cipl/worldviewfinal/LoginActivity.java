package com.cipl.worldviewfinal;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import com.cipl.DataClasses.ApplicationInfo;
import com.cipl.DataClasses.StaticData;
import com.cipl.HierarchyClasses.MakeHierarchy;
import com.cipl.ParserClasses.GroupFieldsData;
import com.cipl.ParserClasses.ParseGlobalTags;
import com.cipl.ParserClasses.ParseHomesafe;
import com.cipl.ParserClasses.ParseRovr;
import com.cipl.ParserClasses.ParsedGroupNameData;
import com.cipl.ParserClasses.URLClass;
import com.cipl.Widgets.CustomProgressDialog;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
public class LoginActivity extends Activity 
{
	ArrayList<GroupFieldsData> sorted_list;
	public LinkedHashSet<String> originalList,changedList ;
	CheckBox chk_box_remember_me;
	Button btn_login;
	EditText edit_user_email,edit_password;
	SharedPreferences loginPref;
	SharedPreferences.Editor prefEditor;
	CustomProgressDialog mCustomProgressDialog;
	public static String username_entered;
	public static String password_entered;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		initialize_ids();
		loginPref = LoginActivity.this.getSharedPreferences(ApplicationInfo.APPLICATION_PREF, MODE_PRIVATE);
		prefEditor = loginPref.edit();
		String username = loginPref.getString("shared_username", "");
		String password = loginPref.getString("shared_password", "");
		edit_user_email.setText(username);
		edit_password.setText(password);
		if(loginPref.getBoolean("shared_stored",false))
		{
			chk_box_remember_me.setChecked(true);
		}

		//		StaticData.groupIdSelected=new ArrayList<String>();
		StaticData.groupIdSelected=new LinkedHashSet<String>();
		StaticData.groupAdded=new LinkedHashSet<String>();
		StaticData.groupDeleted=new LinkedHashSet<String>();
		Intent i=getIntent();
		String status=i.getStringExtra("CONNECTIVITY");
		if(!status.equals("CONNECTED"))
		{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Connection Problem");
			alertDialog.setMessage("You are not connected to Internet");
			alertDialog.setCancelable(false);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int which) {
					LoginActivity.this.finish();
					return;
				}
			});
			alertDialog.show();

		}

		btn_login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				username_entered=edit_user_email.getText().toString();
				password_entered=edit_password.getText().toString();    
				if(chk_box_remember_me.isChecked())
				{
					chk_box_remember_me.setChecked(true);
					loginPref = LoginActivity.this.getSharedPreferences(ApplicationInfo.APPLICATION_PREF, MODE_PRIVATE);
					prefEditor = loginPref.edit();
					prefEditor.putString("shared_username", username_entered);
					prefEditor.putString("shared_password", password_entered);
					prefEditor.putBoolean("shared_stored", true);
					prefEditor.commit();

				}
				else
				{
					loginPref = LoginActivity.this.getSharedPreferences(ApplicationInfo.APPLICATION_PREF, MODE_PRIVATE);
					prefEditor = loginPref.edit();
					prefEditor.putString("shared_username", "");
					prefEditor.putString("shared_password", "");
					prefEditor.putBoolean("shared_stored", false);
					prefEditor.commit();

				}

				try
				{
					if(username_entered.contains("@cartasite.com"))
					{
						if(apps_installed())
							new LoadRefreshedData().execute();
						else
							Toast.makeText(LoginActivity.this, "Install Google Play Services and Enable Location services", Toast.LENGTH_LONG).show();
					}
					else if(username_entered.equals(""))
					{
						if(apps_installed())
						{
							AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
							builder.setMessage("Enter User Name!!!")
							.setCancelable(false)
							.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
							AlertDialog alert = builder.create();
							alert.show();
						}
						else
							Toast.makeText(LoginActivity.this, "Install Google Play Services and Enable Location services", 
									Toast.LENGTH_LONG).show();
					}
					else
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage("Wrong E-Mail!!")
						.setCancelable(false)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
						//	Toast.makeText(LoginActivity.this, "wrong site name", Toast.LENGTH_SHORT).show();
					}
				}
				catch(Exception ex)
				{
					System.out.println("Catch block..........................................running.........Exception.................");
				}
			}
		});
	}

	/**
	 * Method to initialize the views in activity.
	 */
	private void initialize_ids() 
	{
		btn_login=(Button)findViewById(R.id.btn_login_login);
		chk_box_remember_me=(CheckBox)findViewById(R.id.chk_remember_login);
		edit_user_email=(EditText)findViewById(R.id.et_email_login);
		edit_password=(EditText)findViewById(R.id.et_password_login);

		//edit_user_email.setText("gkushnir55@cartasite.com");
	}
	public void onBackPressed() 
	{
		finish();
	}

	/**
	 *Async task class to load refreshed data.
	 *
	 */
	private class LoadRefreshedData extends AsyncTask<String, Void, String>
	{
		@Override
		protected void onPreExecute() {
			System.out.println("Loading..do in background");
			mCustomProgressDialog = CustomProgressDialog.createDialog(LoginActivity.this, "", "");
			mCustomProgressDialog.show();
			mCustomProgressDialog.setCancelable(false);
			System.out.println("In pre execute");
		}
		@Override
		protected String doInBackground(String... params)
		{
			ParsedGroupNameData grpData=new ParsedGroupNameData();
			ParsedGroupNameData.groupStorage.clear();
			grpData.parseGroupList(URLClass.group_url_method(username_entered).replaceAll(" ", "%20").trim());

			sorted_list=new MakeHierarchy().getData();
			StaticData.groupList=sorted_list;
			StaticData.groupAdded.addAll(StaticData.groupIdSelected);


			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			mCustomProgressDialog.dismiss();
			ParseHomesafe.homesafeStorage.clear();
			ParseRovr.rovrStorage.clear();
			ParseGlobalTags.globalTagsStorage.clear();
			StaticData.selectedTab=1;
			startActivity(new Intent(LoginActivity.this,ObjectsActivity.class));

		}
	}
	/**
	 * Method to check weather Google Play Services is installed on device or not. 
	 */
	private boolean apps_installed() {

		boolean play_installed=false;
		boolean location_enabled=false;
		//To check Google play services are installed
		try {
			LoginActivity.this.getPackageManager().getPackageInfo("com.android.vending", 0);
			play_installed=true;
		} 
		catch (NameNotFoundException e) 
		{
			play_installed=false;
		}
		//To check Location Services are enabled
		LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);  
		if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){  
			//GPS enabled
			location_enabled=true;
		}
		else{
			//GPS disabled
			location_enabled=false;
		}
		if(play_installed&&location_enabled)
			return true;

		return false;
	}
}
