package com.cipl.worldviewfinal;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import com.cipl.DataClasses.StaticData;
import com.cipl.HierarchyClasses.MakeHierarchy;
import com.cipl.ParserClasses.CustomGroupAdapter;
import com.cipl.ParserClasses.GroupFieldsData;
import com.cipl.ParserClasses.ParsedGroupNameData;
import com.cipl.ParserClasses.URLClass;
import com.cipl.Widgets.CustomProgressDialog;
/**
 * Activity to show all groups according to the entered user.
 *
 */
public class GroupActivity extends Activity implements OnClickListener
{
	Button btn_done,btn_refresh;
	ListView lv_groups;
	MakeHierarchy mk;
	ArrayList<GroupFieldsData> sorted_list;
	CustomProgressDialog mCustomProgressDialog;
	public LinkedHashSet<String> originalList,changedList ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_group);

		lv_groups=(ListView)findViewById(R.id.groups_list_view);
		btn_done=(Button)findViewById(R.id.btn_done_group);
		btn_refresh=(Button)findViewById(R.id.btn_refresh_group);
		btn_done.setOnClickListener(this);
		btn_refresh.setOnClickListener(this);
		originalList = new  LinkedHashSet<String>();
		originalList.addAll(StaticData.groupIdSelected);
		CustomGroupAdapter adapter=new CustomGroupAdapter(GroupActivity.this,StaticData.groupList);
		lv_groups.setAdapter(adapter);
	}
	/**
	 *     Method to show the data in List view. 
	 */
	private void showListView()
	{
		CustomGroupAdapter adapter=new CustomGroupAdapter(GroupActivity.this,sorted_list);
		lv_groups.setAdapter(adapter);
	}
	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(GroupActivity.this,LoginActivity.class));
		finish();
	}
	//
	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.btn_done_group)
		{
			StaticData.groupAdded.clear();
			StaticData.groupDeleted.clear();
			changedList = new  LinkedHashSet<String>();
			changedList.addAll(StaticData.groupIdSelected);
			StaticData.groupDeleted=new  LinkedHashSet<String>(originalList);

			StaticData.groupDeleted.removeAll(changedList);
			StaticData.groupAdded=new  LinkedHashSet<String>(changedList);
			StaticData.groupAdded.removeAll(originalList);

			Intent i=new Intent(GroupActivity.this,ObjectsActivity.class);


			startActivity(i);
			finish();

		}
		else if(v.getId()==R.id.btn_refresh_group)
		{
			new LoadGroupRefreshedData().execute();
		}

	}
	/**
	 * Async Task Class load Refreshed data from API.
	 *
	 */
	private class LoadGroupRefreshedData extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {

			mCustomProgressDialog = CustomProgressDialog.createDialog(GroupActivity.this, "", "");
			mCustomProgressDialog.show();
			mCustomProgressDialog.setCancelable(false);
			System.out.println("In pre execute");
		}
		@Override
		protected String doInBackground(String... params)
		{
			originalList = new  LinkedHashSet<String>();
			originalList.addAll(StaticData.groupIdSelected);
			ParsedGroupNameData grpData=new ParsedGroupNameData();
			ParsedGroupNameData.groupStorage.clear();

			grpData.parseGroupList(URLClass.group_url_method(LoginActivity.username_entered).replaceAll(" ", "%20").trim());

			mk=new MakeHierarchy();
			sorted_list=mk.getData();
			StaticData.groupList=sorted_list;
			publishProgress();

			System.out.println("Loading.........do in background................in Group");

			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			mCustomProgressDialog.dismiss();
		}
		@Override
		protected void onProgressUpdate(Void...voids) 
		{
			showListView();
		}

	}
}

