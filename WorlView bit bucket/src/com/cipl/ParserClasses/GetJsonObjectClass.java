package com.cipl.ParserClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
/**Class to Get JSON Objects from URL.
 *
 */
public class GetJsonObjectClass {
	/**Method to get JSON Object from URL.
	 * @param url source url for JSON data.
	 * @return JSON Object containing data in JSON Format.
	 */
	public  static  JSONObject  getJSONObjectfromURL(String url) 
	{
		InputStream mIs = null;
		String result = "";
		JSONObject jObjectLogin = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try{
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = bufferReader.readLine()) != null) {
				if(line.trim().equals("\n"))
					continue;
				stringBuilder.append(line + "\n");
			}
			mIs.close();
			result=stringBuilder.toString();
		}catch(Exception e){

			Log.e("log_tag", "Error converting result "+e.toString());
		}
		try {
			jObjectLogin = new JSONObject(result);
		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jObjectLogin;
	}
}
