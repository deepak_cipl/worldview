package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.TripEventTypeData;
import com.cipl.worldviewfinal.LoginActivity;
/**
 *Class to parse Trip Detail JSON Data and return Array List of Trip Detail objects. 
 */
public class ParseTripDetailsData {

	/**Method returning the array list of Trip details Events from the Api.
	 * @param tripid tripid of which detail has to be checked.
	 * @return Array List of all events associated with the trip.
	 */
	public ArrayList<TripEventTypeData> getParseTripDetailsDataList(String tripid)
	{
		ArrayList<TripEventTypeData> tripDetailsDataList=new ArrayList<TripEventTypeData>();

		String url=URLClass.tripDetailEventsData_url_method(tripid, LoginActivity.username_entered);

		JSONObject jsonObject;
		JSONArray jsonArray;

		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);

			for(int i=0;i<jsonArray.length();i++)
			{

				TripEventTypeData tripDetailData = new TripEventTypeData();

				jsonObject=jsonArray.getJSONObject(i);
				String recordID=jsonObject.getString("RecordID");
				tripDetailData.setRecordID(recordID);
				String assetID=jsonObject.getString("AssetID");
				tripDetailData.setAssetID(assetID);
				String personID=jsonObject.getString("PersonID");
				tripDetailData.setPersonID(personID);
				String eventTypeID=jsonObject.getString("EventTypeID");
				tripDetailData.setEventTypeID(eventTypeID);
				String deviceID=jsonObject.getString("DeviceID");
				tripDetailData.setDeviceID(deviceID);
				String address=jsonObject.getString("Address");
				tripDetailData.setAddress(address);
				double latitude=Double.parseDouble(jsonObject.getString("Latitude"));
				tripDetailData.setLatitude(latitude);
				double longitude=Double.parseDouble(jsonObject.getString("Longitude"));
				tripDetailData.setLongitude(longitude);
				String deviceTypeID=jsonObject.getString("DeviceTypeID");
				tripDetailData.setDeviceTypeID(deviceTypeID);
				String source=jsonObject.getString("Source");
				tripDetailData.setSource(source);
				String eSN=jsonObject.getString("ESN");
				tripDetailData.setESN(eSN);
				String personName=jsonObject.getString("PersonName");
				tripDetailData.setPersonName(personName);
				String assetName=jsonObject.getString("AssetName");
				tripDetailData.setAssetName(assetName);
				String eventTypeName=jsonObject.getString("EventTypeName");
				tripDetailData.setEventTypeName(eventTypeName);
				String eventTime=jsonObject.getString("EventTime");
				tripDetailData.setEventTime(eventTime);
				String acceleration=jsonObject.getString("Acceleration");
				tripDetailData.setAcceleration(acceleration);
				String speed=jsonObject.getString("Speed");
				tripDetailData.setSpeed(speed);
				String bearing=jsonObject.getString("Bearing");
				tripDetailData.setBearing(bearing);
				String accelDuration=jsonObject.getString("AccelDuration");
				tripDetailData.setAccelDuration(accelDuration);


				tripDetailsDataList.add(tripDetailData);					

			}
			return tripDetailsDataList;
		}catch (JSONException e) {
			return tripDetailsDataList;

		}

	}
}