package com.cipl.indexingclasses;

import java.util.Comparator;

/**
 *Comparator Class for Global Tag.
 *
 */
public class GlobalTagItemComparator implements Comparator<GlobalTagsItemInterface> {

	@Override
	public int compare(GlobalTagsItemInterface lhs, GlobalTagsItemInterface rhs) {
		if(lhs.getItemForIndex() == null || rhs.getItemForIndex() == null)
			return -1;


		return(lhs.getItemForIndex().compareTo(rhs.getItemForIndex() ) );

	}
}

