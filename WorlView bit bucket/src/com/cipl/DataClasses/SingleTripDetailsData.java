package com.cipl.DataClasses;

/**
 * Data Class for Trip Detail.
 *
 */
public class SingleTripDetailsData 
{
	private String recordID;
	private String assetID;
	private String personID;
	private String eventTypeID;
	private String deviceID;
	private String address;
	private String latitude;
	private String longitude;
	private String deviceTypeID;
	private String source;
	private String ESN;
	private String personName;
	private String assetName;
	private String eventTypeName;
	private String eventTime;
	private String acceleration;
	private String speed;
	private String bearing;
	private String accelDuration;
	/*
	 * Setter(to set the value of the fields in object) and Getter(to get the fields values from the object)
	 *  methods for the homeSAFE Data object.
	 * */
	public String getRecordID() {
		return recordID;
	}
	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}
	public String getAssetID() {
		return assetID;
	}
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}
	public String getPersonID() {
		return personID;
	}
	public void setPersonID(String personID) {
		this.personID = personID;
	}
	public String getEventTypeID() {
		return eventTypeID;
	}
	public void setEventTypeID(String eventTypeID) {
		this.eventTypeID = eventTypeID;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getDeviceTypeID() {
		return deviceTypeID;
	}
	public void setDeviceTypeID(String deviceTypeID) {
		this.deviceTypeID = deviceTypeID;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getESN() {
		return ESN;
	}
	public void setESN(String eSN) {
		ESN = eSN;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getEventTypeName() {
		return eventTypeName;
	}
	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(String acceleration) {
		this.acceleration = acceleration;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getBearing() {
		return bearing;
	}
	public void setBearing(String bearing) {
		this.bearing = bearing;
	}
	public String getAccelDuration() {
		return accelDuration;
	}
	public void setAccelDuration(String accelDuration) {
		this.accelDuration = accelDuration;
	}

}
