package com.cipl.Widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import com.cipl.indexingclasses.*;

/**
 *Class for Customized List View for ROVR.
 *
 */
public class ExtendedRovrListView extends RovrListView {

	public ExtendedRovrListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void createScroller(){

		mScroller = new IndexScroller(getContext(), this);

		mScroller.setAutoHide(autoHide);

		// style 2 
		mScroller.setShowIndexContainer(true);
		mScroller.setIndexPaintColor(Color.WHITE);

		if(autoHide)
			mScroller.hide();
		else
			mScroller.show();

	}
}
