package com.cipl.indexingclasses;

import java.util.Comparator;
/**
 *Comparator Class of ROVR Item data to sort data alphabetically.
 */
public class RovrItemComparator implements Comparator<RovrItemInterface> {

	@Override
	public int compare(RovrItemInterface lhs, RovrItemInterface rhs) {
		if(lhs.getItemForIndex() == null || rhs.getItemForIndex() == null)
			return -1;


		return(lhs.getItemForIndex().compareTo(rhs.getItemForIndex() ) );

	}

}
