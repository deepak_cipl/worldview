package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.StaticData;

/**
 *Class to parse Group JSON Data and return Array List of group objects. 
 */
public class ParsedGroupNameData 
{
	public static ArrayList<GroupFieldsData> groupStorage = new ArrayList<GroupFieldsData>();

	/**
	 * Method to parse Group Data in Static Array List. 
	 * @param url
	 */
	public void parseGroupList(String url)
	{
		JSONObject jsonObject;
		JSONArray jsonArray;

		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{
				return ;
			}
			else
			{

				for(int i=0;i<jsonArray.length();i++)
				{

					GroupFieldsData gData = new GroupFieldsData();

					jsonObject=jsonArray.getJSONObject(i);

					String group_id=jsonObject.getString("GroupID");
					gData.setGroupId(group_id);

					String group_name=jsonObject.getString("GroupName");
					gData.setGroupName(group_name);

					String parent_group_id=jsonObject.getString("ParentGroupID");
					gData.setParentGroupId(parent_group_id);

					groupStorage.add(gData);					

				}
			}

		}
		catch (JSONException e) {


		}
		updateGroupSelectState();
	}
	/**
	 * Set the selected states of group list according to previously selected state of the groups.
	 */
	private void updateGroupSelectState(){

		ArrayList<GroupFieldsData> tempGroupStorage=new ArrayList<GroupFieldsData>();
		ArrayList<String> tempSelectedGroupId=new ArrayList<String>();
		tempGroupStorage.addAll(groupStorage);
		tempSelectedGroupId.addAll(StaticData.groupIdSelected);
		groupStorage.clear();
		//		StaticData.groupIdSelected.clear();
		for(int i=0;i<tempSelectedGroupId.size();i++)
		{
			for(int j=0;j<tempGroupStorage.size();j++)
			{
				if(tempSelectedGroupId.get(i)==tempGroupStorage.get(j).getGroupId()){
					tempGroupStorage.get(j).setSelected(true);
					StaticData.groupIdSelected.add(tempSelectedGroupId.get(i));
					break;
				}

			}

		}
		groupStorage.addAll(tempGroupStorage);
	}
}
