package com.cipl.ParserClasses;

import java.util.ArrayList;
import java.util.Collections;

import com.cipl.DataClasses.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.cipl.indexingclasses.*;
/**
 *Class to parse Group JSON Data and return Array List of ROVR objects. 
 */
public class ParseRovr 
{
	public static ArrayList<RovrItemInterface> rovrStorage = new ArrayList<RovrItemInterface>();
	public static ArrayList<RovrData> rovrStaticData=new ArrayList<RovrData>();
	/**
	 * Method to parse ROVR data in Static Array List. 
	 * @param url
	 */
	public ArrayList<RovrItemInterface>  parseRovrList(String url)
	{
		ArrayList<RovrItemInterface> rovrData=new ArrayList<RovrItemInterface>();
		JSONObject jsonObject;
		JSONArray jsonArray;

		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{
				return null;
			}
			else
			{

				for(int i=0;i<jsonArray.length();i++)
				{

					RovrData rData = new RovrData();

					jsonObject=jsonArray.getJSONObject(i);

					String object_type=jsonObject.getString("ObjectType");
					rData.setObject_type(object_type);

					String device_id=jsonObject.getString("DeviceID");
					rData.setDevice_id(device_id);

					String device_name=jsonObject.getString("DeviceName");
					rData.setDevice_name(device_name);

					String asset_id=jsonObject.getString("AssetID");
					rData.setAsset_id(asset_id);

					String asset_name=jsonObject.getString("AssetName");
					rData.setAsset_name(asset_name);

					String person_id=jsonObject.getString("PersonId");
					rData.setPerson_id(person_id);

					String person_name=jsonObject.getString("PersonName");
					rData.setPerson_name(person_name);

					String object_event_type=jsonObject.getString("ObjectEventType");
					rData.setObject_event_type(object_event_type);

					String ObjectOverallScore=jsonObject.getString("ObjectOverallScore");
					if((ObjectOverallScore.equals("null"))||(ObjectOverallScore.equals("")))
					{
						ObjectOverallScore="0";
					}
					rData.setObjectOverallScore(ObjectOverallScore);

					String ObjectDistanceDriven=jsonObject.getString("ObjectDistanceDriven");
					rData.setObjectDistanceDriven(ObjectDistanceDriven);
					String ObjectTotalHoursDriven=jsonObject.getString("ObjectTotalHoursDriven");
					rData.setObjectTotalHoursDriven(ObjectTotalHoursDriven);
					String ObjectTotalMinutesIdling=jsonObject.getString("ObjectTotalMinutesIdling");
					rData.setObjectTotalMinutesIdling(ObjectTotalMinutesIdling);


					rovrData.add(rData);
					//					rovrStorage.add(rData);	
					rovrStaticData.add(rData);

				}
				Collections.sort(rovrStaticData);
			}

		}
		catch (JSONException e) {

		}
		return rovrData;
	}

}
