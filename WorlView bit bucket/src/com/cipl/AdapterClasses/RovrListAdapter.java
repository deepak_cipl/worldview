package com.cipl.AdapterClasses;

import java.util.Collections;
import java.util.List;

import com.cipl.DataClasses.StaticData;
import com.cipl.indexingclasses.*;
import com.cipl.worldviewfinal.R;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 *Customized Array Adapter class to show Objects list in Alphabetical order and divided by sections. 
 *
 */
public class RovrListAdapter extends ArrayAdapter<RovrItemInterface>{

	private int resource; // store the resource layout id for 1 row
	private boolean inSearchMode = false;
	List<RovrItemInterface> items_list;
	private RovrSectionIndexer indexer = null;
	public RovrListAdapter(Context _context, int _resource, List<RovrItemInterface> _items) {
		super(_context, _resource, _items);
		resource = _resource;
		items_list=_items;
		// need to sort the items array first, then pass it to the indexer
		Collections.sort(_items, new RovrItemComparator());
		setIndexer(new RovrSectionIndexer(_items));

	}

	// get the section TextView from row view
	// the section view will only be shown for the first item
	/**Method to return text view for section.
	 * @param rowView
	 * @return text view returned from Row View.
	 */
	public TextView getSectionTextView(View rowView){
		TextView sectionTextView = (TextView)rowView.findViewById(R.id.sectionTextView);
		return sectionTextView;
	}
	/**Method to show section Alphabet item at before the first Item.
	 * @param rowView Parent view
	 * @param item 
	 * @param position position of view
	 */
	public void showSectionViewIfFirstItem(View rowView, RovrItemInterface item, int position){
		TextView sectionTextView = getSectionTextView(rowView);

		// if in search mode then dun show the section header
		if(inSearchMode){
			sectionTextView.setVisibility(View.GONE);
		}
		else
		{
			// if first item then show the header

			if(indexer.isFirstItemInSection(position)){

				String sectionTitle = indexer.getSectionTitle(item.getItemForIndex());
				sectionTextView.setText(sectionTitle);
				sectionTextView.setVisibility(View.VISIBLE);
			}
			else
				sectionTextView.setVisibility(View.GONE);
		}
	}

	/**do all the data population for the row here
    subclass overwrite this to draw more items.
	 * @param parentView
	 * @param item
	 * @param position
	 */
	public void populateDataForRow(View parentView, RovrItemInterface item , int position){
		// default just draw the item only
		View infoView = parentView.findViewById(R.id.infoRowContainer);

		TextView nameView = (TextView)infoView.findViewById(R.id.tv_row_object);
		nameView.setText(item.getItemForIndex());
	}

	// this should be override by subclass if necessary
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		ViewGroup parentView;
		RovrItemInterface item = getItem(position);

		//Log.i("ContactListAdapter", "item: " + item.getItemForIndex());


		parentView = new LinearLayout(getContext()); // Assumption: the resource parent id is a linear layout
		String inflater = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
		vi.inflate(resource, parentView, true);

		parentView = (LinearLayout) parentView;



		//		 for the very first section item, we will draw a section on top
		showSectionViewIfFirstItem(parentView, item, position);

		// set row items here
		populateDataForRow(parentView, item, position);

		return parentView;

	}

	/**Check if the List is in search mode.
	 * @return True if the list is in search mode.
	 */
	public boolean isInSearchMode() {
		return inSearchMode;
	}

	/**Set the list into search mode.
	 * @param inSearchMode
	 */
	public void setInSearchMode(boolean inSearchMode) {
		try{
			if(inSearchMode)
				StaticData.rovrListInSearchMode=items_list;
			else
				StaticData.rovrListInSearchMode.clear();
			this.inSearchMode = inSearchMode;
		}
		catch(Exception e){

		}	
	}

	public RovrSectionIndexer getIndexer() {
		return indexer;
	}
	/**set the indexer bar.
	 * @param RovrSectionIndexer
	 */
	public void setIndexer(RovrSectionIndexer indexer) {
		this.indexer = indexer;
	}
	/*
	 * */
}

