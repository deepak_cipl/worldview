package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.TripData;
import com.cipl.DataClasses.TripEventTypeData;

/**
 * Class to parse recent trip data of object.
 *
 */
public class ParseRecentTripData {

	/**
	 * Method to parse HomeSafe data in Static Array List. 
	 * @param url
	 */
	public TripData parseRecentTrip(String url)
	{
		JSONObject jsonObject;
		JSONArray jsonArray;
		TripData tripData = new TripData();
		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{

				return tripData ;
			}
			else
			{
				jsonObject=jsonArray.getJSONObject(0);
				String tripID=jsonObject.getString("TripID");
				tripData.setTripID(tripID);
				String assetID=jsonObject.getString("AssetID");
				tripData.setAssetID(assetID);
				String personID=jsonObject.getString("PersonID");
				tripData.setPersonID(personID);
				String deviceID=jsonObject.getString("DeviceID");
				tripData.setDeviceID(deviceID);
				double startingLat=Double.parseDouble(jsonObject.getString("StartingLat"));
				tripData.setStartingLat(startingLat);
				double  startingLon=Double.parseDouble(jsonObject.getString("StartingLon"));
				tripData.setStartingLon(startingLon);
				double endingLat=Double.parseDouble(jsonObject.getString("EndingLat"));
				tripData.setEndingLat(endingLat);
				double endingLon=Double.parseDouble(jsonObject.getString("EndingLon"));
				tripData.setEndingLon(endingLon);
				String startingTime=jsonObject.getString("StartingTime");
				tripData.setStartingTime(startingTime);
				String endingTime=jsonObject.getString("EndingTime");
				tripData.setEndingTime(endingTime);
				String startingAddress=jsonObject.getString("StartingAddress");
				tripData.setStartingAddress(startingAddress);
				String endingAddress=jsonObject.getString("EndingAddress");
				tripData.setEndingAddress(endingAddress);
				String duration=jsonObject.getString("Duration");
				tripData.setDuration(duration);
				String eSN=jsonObject.getString("ESN");
				tripData.setESN(eSN);
				String personName=jsonObject.getString("PersonName");
				tripData.setPersonName(personName);
				String assetName=jsonObject.getString("AssetName");
				tripData.setAssetName(assetName);
				String distance=jsonObject.getString("Distance");
				tripData.setDistance(distance);
				String TripOverallScore=jsonObject.getString("TripOverallScore");
				tripData.setTripOverallScore(Double.parseDouble(TripOverallScore));
				String AverageSpeed=jsonObject.getString("AverageSpeed");
				tripData.setAverageSpeed(AverageSpeed);
				String MaxSpeed=jsonObject.getString("MaxSpeed");
				tripData.setMaxSpeed(MaxSpeed);
				String BrakingEventsCount=jsonObject.getString("BrakingEventsCount");
				tripData.setBrakingEventsCount(Integer.parseInt(BrakingEventsCount));
				String SpeedingEventsCount=jsonObject.getString("SpeedingEventsCount");
				tripData.setSpeedingEventsCount(Integer.parseInt(SpeedingEventsCount));
				String FastAccelEventsCount=jsonObject.getString("FastAccelEventsCount");
				tripData.setFastAccelEventsCount(Integer.parseInt(FastAccelEventsCount));
				ArrayList<TripEventTypeData> tripDetails=new ParseTripDetailsData().getParseTripDetailsDataList(tripID);
				tripData.setTripDetails(tripDetails);
			}

		}
		catch (JSONException e) {

		}
		return tripData;
	}
}
