package com.cipl.ParserClasses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.DataClasses.TripData;
import com.cipl.DataClasses.TripEventTypeData;
import com.cipl.worldviewfinal.LoginActivity;
/**
 *Class to parse Trip JSON Data and return Array List of Trip Data objects. 
 */
public class ParseTripData {


	public ArrayList<TripData> getParseTripDataList(String deviceid,String assetid,String personid,String date)
	{
		ParseTripDetailsData parseTripDetailsData=new ParseTripDetailsData();

		ArrayList<TripData> tripDataList=new ArrayList<TripData>();

		String url=URLClass.tripData_url_method(deviceid, assetid, personid, LoginActivity.username_entered, date, date);
		System.out.println("Calling Trip Api........."+URLClass.tripData_url_method(deviceid, assetid, personid, LoginActivity.username_entered, date, date));

		JSONObject jsonObject;
		JSONArray jsonArray;

		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);

			for(int i=0;i<jsonArray.length();i++)
			{

				TripData tripData = new TripData();

				jsonObject=jsonArray.getJSONObject(i);

				String tripID=jsonObject.getString("TripID");
				tripData.setTripID(tripID);
				String assetID=jsonObject.getString("AssetID");
				tripData.setAssetID(assetID);
				String personID=jsonObject.getString("PersonID");
				tripData.setPersonID(personID);
				String deviceID=jsonObject.getString("DeviceID");
				tripData.setDeviceID(deviceID);
				double startingLat=Double.parseDouble(jsonObject.getString("StartingLat"));
				tripData.setStartingLat(startingLat);
				double  startingLon=Double.parseDouble(jsonObject.getString("StartingLon"));
				tripData.setStartingLon(startingLon);
				double endingLat=Double.parseDouble(jsonObject.getString("EndingLat"));
				tripData.setEndingLat(endingLat);
				double endingLon=Double.parseDouble(jsonObject.getString("EndingLon"));
				tripData.setEndingLon(endingLon);
				String startingTime=jsonObject.getString("StartingTime");
				tripData.setStartingTime(startingTime);
				String endingTime=jsonObject.getString("EndingTime");
				tripData.setEndingTime(endingTime);
				String startingAddress=jsonObject.getString("StartingAddress");
				tripData.setStartingAddress(startingAddress);
				String endingAddress=jsonObject.getString("EndingAddress");
				tripData.setEndingAddress(endingAddress);
				String duration=jsonObject.getString("Duration");
				tripData.setDuration(duration);
				String eSN=jsonObject.getString("ESN");
				tripData.setESN(eSN);
				String personName=jsonObject.getString("PersonName");
				tripData.setPersonName(personName);
				String assetName=jsonObject.getString("AssetName");
				tripData.setAssetName(assetName);
				String distance=jsonObject.getString("Distance");
				tripData.setDistance(distance);
				String TripOverallScore=jsonObject.getString("TripOverallScore");
				tripData.setTripOverallScore(Double.parseDouble(TripOverallScore));
				String AverageSpeed=jsonObject.getString("AverageSpeed");
				tripData.setAverageSpeed(AverageSpeed);
				String MaxSpeed=jsonObject.getString("MaxSpeed");
				tripData.setMaxSpeed(MaxSpeed);
				String BrakingEventsCount=jsonObject.getString("BrakingEventsCount");
				tripData.setBrakingEventsCount(Integer.parseInt(BrakingEventsCount));
				String SpeedingEventsCount=jsonObject.getString("SpeedingEventsCount");
				tripData.setSpeedingEventsCount(Integer.parseInt(SpeedingEventsCount));
				String FastAccelEventsCount=jsonObject.getString("FastAccelEventsCount");
				tripData.setFastAccelEventsCount(Integer.parseInt(FastAccelEventsCount));

				ArrayList<TripEventTypeData> tripDetails=parseTripDetailsData.getParseTripDetailsDataList(tripID);

				tripData.setTripDetails(tripDetails);



				tripDataList.add(tripData);					

			}
			return tripDataList;
		}catch (JSONException e) {
			return tripDataList;

		}



	}
}
