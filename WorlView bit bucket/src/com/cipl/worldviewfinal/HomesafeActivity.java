package com.cipl.worldviewfinal;

import com.cipl.DataClasses.StaticData;
import com.cipl.DataClasses.TripData;
import com.cipl.ParserClasses.ParseRecentLocation;
import com.cipl.ParserClasses.ParseRecentTripData;
import com.cipl.ParserClasses.URLClass;
import com.cipl.Widgets.CustomProgressDialog;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 *Activity to show map according to the object selected by the user.
 *
 */
public class HomesafeActivity extends FragmentActivity implements OnClickListener,LocationListener {
	CustomProgressDialog mCustomProgressDialog;
	LinearLayout parent_to_hide_homesafe;
	TextView textViewName,textViewTitle;
	GoogleMap map;
	Marker curLoc,resLoc,locMarker_starting,locMarker_current;
	Button btn_done_HomesafeActivity,btn_referesh;
	String personId;
	String deviceId;
	String assetId,name;
	boolean firstTimeZoom=true;
	String fromActivity;
	TripData tripdata;
	RefereshThread threadreferesh;
	boolean stopThread=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_homesafe);

		homesafeInitialization();
		threadreferesh=new RefereshThread();
		threadreferesh.start();
		new LoadRecentlocationData().execute();
	}
	@Override
	protected void onStop() {
		super.onStop();
		stopThread=true;
		//		threadreferesh.stop();
	}
	/**
	 *Method to be called when bottom bar of Information pane clicked. 
	 */

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_done_homesafe:
			Intent intent=new Intent(HomesafeActivity.this,ObjectsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//			intent.putExtra("selected_tab_sent",2);
			StaticData.selectedTab=2;
			startActivity(intent);
			finish();

			break;
		case R.id.btn_refresh_homesafe:
			new LoadRefereshedRecentlocationData().execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() 
	{
		Intent intent=new Intent(HomesafeActivity.this,ObjectsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//			intent.putExtra("selected_tab_sent",2);
		StaticData.selectedTab=2;
		startActivity(intent);
		finish();
	}
	/**
	 *method to initialize map when homesafe and globalTag  object is selected.
	 */
	private void activate_map()
	{
		map.clear();
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, true);

		Location location = locationManager.getLastKnownLocation(provider);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0,0) , 14.0f)) ;
		//		map.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
		if(location!=null){
			onLocationChanged(location);
		}
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,2000, 0, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2000, 0, this);
		locationManager.requestLocationUpdates(provider, 60000, 0, this);

		String latitude=ParseRecentLocation.recentLocationStorage.get(0).getLatitude();
		String longitude=ParseRecentLocation.recentLocationStorage.get(0).getLongitude();
		double lat=Double.parseDouble(latitude);
		double longi=Double.parseDouble(longitude);
		final LatLng RECENT_LOC = new LatLng(lat,longi);

		resLoc = map.addMarker(new MarkerOptions().position(RECENT_LOC).title("Last Location")
				.snippet(latitude+","+longitude).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

		// Getting LocationManager object from System Service LOCATION_SERVICE
		// Move the camera instantly to hamburg with a zoom of 15.
		//			map.moveCamera(CameraUpdateFactory.newLatLngZoom(RECENT_LOC, 18));

		// Zoom in, animating the camera.

	}
	/**
	 *Method to initialize map when ROVR object is selected.
	 */

	private void activate_rovr_map()
	{

		map.clear();
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, true);

		Location location = locationManager.getLastKnownLocation(provider);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0,0) , 14.0f)) ;
		//		map.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
		if(location!=null){
			onLocationChanged(location);
		}
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,2000, 0, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2000, 0, this);
		locationManager.requestLocationUpdates(provider, 60000, 0, this);

		double lat=tripdata.getStartingLat();
		double longi=tripdata.getStartingLon();
		LatLng location1 = new LatLng(lat,longi);

		locMarker_starting = map.addMarker(new MarkerOptions().position(location1).position(location1).title("Starting")
				.snippet(tripdata.getStartingAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));


		lat=tripdata.getEndingLat();
		longi=tripdata.getEndingLon();
		location1 = new LatLng(lat,longi);
		locMarker_current = map.addMarker(new MarkerOptions().position(location1).title("Ending")
				.snippet(tripdata.getEndingAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

		// Getting LocationManager object from System Service LOCATION_SERVICE
		// Move the camera instantly to hamburg with a zoom of 15.
		//			map.moveCamera(CameraUpdateFactory.newLatLngZoom(location1, 18));

		// Zoom in, animating the camera.
		//			map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
	}

	/**
	 *Method to Initialize Activity. 
	 */
	private void homesafeInitialization()
	{
		Bundle b=getIntent().getExtras();
		name=b.getString("name");
		personId=b.getString("personId");
		deviceId=b.getString("deviceId");
		assetId=b.getString("assetId");
		fromActivity=b.getString("fromActivity");
		textViewName=(TextView)findViewById(R.id.tv_homesafe_title);

		textViewName.setText(name);
		textViewTitle=(TextView)findViewById(R.id.tv_activity_title);
		if(fromActivity.equals("ROVR"))
			textViewTitle.setText("ROVR");
		else if(fromActivity.equals("homeSAFE"))
			textViewTitle.setText("homeSAFE");
		else if(fromActivity.equals("globalTAG"))
			textViewTitle.setText("globalTAG");
		parent_to_hide_homesafe=(LinearLayout)findViewById(R.id.parent_bar);
		btn_referesh=(Button)findViewById(R.id.btn_refresh_homesafe);
		btn_referesh.setOnClickListener(this);
		btn_done_HomesafeActivity=(Button)findViewById(R.id.btn_done_homesafe);
		btn_done_HomesafeActivity.setOnClickListener(this);
		map=((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		//map.setMyLocationEnabled(true);
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	}

	/**
	 * Async task class to load recent location.
	 *
	 */
	private class LoadRecentlocationData extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {

		}
		@Override
		protected String doInBackground(String... arg0) {
			if(fromActivity.equals("ROVR")){
				ParseRecentTripData prt=new ParseRecentTripData();

				tripdata=prt.parseRecentTrip(URLClass.recentTripData_url_method("646","null" , "null",LoginActivity.username_entered ).replaceAll(" ", "%20").trim());
				System.out.println("Calling Recent Trip Api::::::::::   "+URLClass.recentTripData_url_method("646","null" , "null",LoginActivity.username_entered ).replaceAll(" ", "%20").trim());
			}else{
				ParseRecentLocation pr=new ParseRecentLocation();
				pr.parseRecentLocationList(URLClass.recentLocationData_url_method("646","null" , "null",LoginActivity.username_entered ).replaceAll(" ", "%20").trim());
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			//			mCustomProgressDialog.dismiss();
			if(fromActivity.equals("ROVR"))
				activate_rovr_map();
			else
				activate_map();
		}

	}

	/**
	 * Async task class to load refreshed recent location.
	 *
	 */
	private class LoadRefereshedRecentlocationData extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {

		}
		@Override
		protected String doInBackground(String... arg0) {
			if(fromActivity.equals("ROVR")){
				ParseRecentTripData prt=new ParseRecentTripData();
				tripdata=prt.parseRecentTrip(URLClass.recentTripData_url_method("646","null" , "null",LoginActivity.username_entered ).replaceAll(" ", "%20").trim());

			}
			else{
				ParseRecentLocation pr=new ParseRecentLocation();
				pr.parseRecentLocationList(URLClass.recentLocationData_url_method("646","null" , "null",LoginActivity.username_entered ).replaceAll(" ", "%20").trim());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			//			mCustomProgressDialog.dismiss();
			if(fromActivity.equals("ROVR")){
				locMarker_current.remove();
				locMarker_starting.remove();
				double lat=tripdata.getStartingLat();
				double longi=tripdata.getStartingLon();
				LatLng location1 = new LatLng(lat,longi);

				locMarker_starting = map.addMarker(new MarkerOptions().position(location1).position(location1).title("Starting Trip Point")
						.snippet(tripdata.getStartingAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));


				lat=tripdata.getEndingLat();
				longi=tripdata.getEndingLon();
				location1 = new LatLng(lat,longi);
				locMarker_current = map.addMarker(new MarkerOptions().position(location1).title("Ending Trip Point")
						.snippet(tripdata.getEndingAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
			}
			else{
				resLoc.remove();
				String latitude=ParseRecentLocation.recentLocationStorage.get(0).getLatitude();
				String longitude=ParseRecentLocation.recentLocationStorage.get(0).getLongitude();
				double lat=Double.parseDouble(latitude);
				double longi=Double.parseDouble(longitude);
				final LatLng RECENT_LOC = new LatLng(lat,longi);

				resLoc = map.addMarker(new MarkerOptions().position(RECENT_LOC)
						.snippet(latitude+","+longitude).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
			}

			System.out.println("::::::::::::::::::::::::::::::::::::Data refereshed:::::::::::::::::::::::::");
		}
	}
	/**
	 * Thread to Call refreshed location from API.
	 *
	 */
	private class RefereshThread extends Thread{
		@Override
		public void run(){
			for(;!stopThread;){
				try {
					Thread.sleep(60000);
					new LoadRefereshedRecentlocationData().execute();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
	//	void reverseGeoCodeMathod(double lattitude,double longitude)
	//	{
	//
	//		Geocoder myLocation = new Geocoder(getApplicationContext(), Locale.getDefault());
	//		try {
	//			if(myLocation.isPresent())
	//			{
	//				List<Address> addresses=null ;
	//				addresses = myLocation.getFromLocation(lattitude,longitude, 1);
	//				System.out.println(".................."+addresses);
	//				StringBuilder sb = new StringBuilder();
	//				if (addresses.size() > 0) 
	//				{
	//					Address address = addresses.get(0);
	//					sb.append(address.getAddressLine(0)+", ");
	//					sb.append(address.getAddressLine(1));
	//				
	//				//	Toast.makeText(getApplicationContext(), sb,Toast.LENGTH_LONG).show();
	//				}
	//			}
	//			else
	//			{
	//			}
	//		}
	//		catch (IOException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//	}

	@Override
	public void onLocationChanged(Location location) {
		// Getting latitude of the current location
		double latitude = location.getLatitude();
		// Getting longitude of the current location
		double longitude = location.getLongitude();
		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(latitude, longitude);
		// Showing the current location in Google Map
		if(firstTimeZoom)
			map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		//		map.clear();
		if(curLoc!=null)
			curLoc.remove();
		curLoc = map.addMarker(new MarkerOptions().position(latLng).title("I am here!!"));
		// Zoom in the Google Map
		//						map.animateCamera(CameraUpdateFactory.zoomTo(15));
		firstTimeZoom=false;

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}



