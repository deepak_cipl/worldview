package com.cipl.ParserClasses;

import java.util.ArrayList;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.cipl.DataClasses.*;
import com.cipl.indexingclasses.HomesafeItemInterface;
/**
 *Class to parse Group JSON Data and return Array List of HomeSafe objects. 
 */
public class ParseHomesafe 
{
	public static ArrayList<HomesafeItemInterface> homesafeStorage = new ArrayList<HomesafeItemInterface>();
	public static ArrayList<HomesafeData> homesafeStaticData=new ArrayList<HomesafeData>();
	/**
	 * Method to parse HomeSafe data in Static Array List. 
	 * @param url
	 */
	public ArrayList<HomesafeItemInterface> parseHomesafeList(String url)
	{
		JSONObject jsonObject;
		JSONArray jsonArray;
		ArrayList<HomesafeItemInterface> homesafeData = new ArrayList<HomesafeItemInterface>(); 
		try
		{
			jsonArray=GetJsonArrayClass.getJSONArrayfromURL(url);
			if(jsonArray==null)
			{
				return null;
			}
			else
			{

				for(int i=0;i<jsonArray.length();i++)
				{


					HomesafeData hData = new HomesafeData();

					jsonObject=jsonArray.getJSONObject(i);

					String object_type=jsonObject.getString("ObjectType");
					hData.setObject_type(object_type);

					String device_id=jsonObject.getString("DeviceID");
					hData.setDevice_id(device_id);

					String device_name=jsonObject.getString("DeviceName");
					hData.setDevice_name(device_name);

					String asset_id=jsonObject.getString("AssetID");
					hData.setAsset_id(asset_id);

					String asset_name=jsonObject.getString("AssetName");
					hData.setAsset_name(asset_name);

					String person_id=jsonObject.getString("PersonId");
					hData.setPerson_id(person_id);

					String person_name=jsonObject.getString("PersonName");
					hData.setPerson_name(person_name);

					String object_event_type=jsonObject.getString("ObjectEventType");
					hData.setObject_event_type(object_event_type);

					String ObjectOverallScore=jsonObject.getString("ObjectOverallScore");
					hData.setObjectOverallScore(ObjectOverallScore);

					String ObjectDistanceDriven=jsonObject.getString("ObjectDistanceDriven");
					hData.setObjectDistanceDriven(ObjectDistanceDriven);
					String ObjectTotalHoursDriven=jsonObject.getString("ObjectTotalHoursDriven");
					hData.setObjectTotalHoursDriven(ObjectTotalHoursDriven);
					String ObjectTotalMinutesIdling=jsonObject.getString("ObjectTotalMinutesIdling");
					hData.setObjectTotalMinutesIdling(ObjectTotalMinutesIdling);

					homesafeData.add(hData);
					//					homesafeStorage.add(hData);
					homesafeStaticData.add(hData);

				}
				Collections.sort(homesafeStaticData); 

			}

		}
		catch (JSONException e) {

		}
		return homesafeData;
	}

}
